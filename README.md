# Proyecto de aprendizaje del estándar HL7 CDA R2

* **Versión del documento:** 0.1 - Prototipo
* **Autor:** Mario Enrique Cortés M <mario.cortes@interoperar.com>
* **Compañía:** Interoperar.
* **Fecha:** 2018-08-07
* **Alcance del proyecto:** Aprendizaje.

## Detalle

Este proyecto contiene ejemplos de instancias de documentos clínicos electrónicos, de conformidad con el estándar HL7 CDA R2.

Los documentos CDA almacenados en éste repositorio son únicamente para fines de aprendizaje.
El diseño de plantillas de documentos CDA para fines implementación en producción, requiere la aprobación concensual de una comunidad de usuarios de HL7.

El alcance de este proyecto, es familiarizar a los participantes con la estructura general de los documentos CDA (CDA Header y CDA Body).  

De acuerdo con la especificación del estándar HL7 CDA R2, La Arquitectura de Documentos Clínicos o CDA (Clinical Document Architecture) es un estándar de marcado de documentos que especifica la estructura y semántica de "*Documentos Clínicos*", con el propósito de intercambio electrónico [^1].

Un *Documento Clínico* es una recopilación formal de observaciones clínica y servicios asistenciales, con las siguientes características [^2]:

- Persistencia.
- Administración y custodia.
- Potencial de autenticación.
- Contexto.
- Integridad.
- Legibilidad humana

## Tipos de documentos:

Las características generales (CDA Header y CDA Body) que comparte con los tipos de documentos que han sido diseñados para este proyecto de aprendizaje, son abordados en la guía de implementación general [CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo) (versión Wiki).

Los tipos de documentos definidos para este proyecto de aprendizaje, denominado **Historia Clínica Electrónica Compartida** son:

TÍTULO | CÓDIGO (LOINC) | NOMBRE
:------:|:--------------:|:------:
HISTORIA CLINICA ELECTRONICA COMPARTIDA DE LABORATORIO | 11502-2 | Laboratory report
HISTORIA CLINICA ELECTRONICA COMPARTIDA DE AYUDAS DIAGNOSTICA DE IMAGENOLOGIA | 18748-4 | Diagnostic imaging study
HISTORIA CLINICA ELECTRONICA COMPARTIDA DE VACUNACION | 82593-5 | Immunization summary report.

Para ver las guías de implementación de cada uno de los tipos de documentos, siga los siguientes enlaces (versión Wiki):

- HISTORIA CLINICA ELECTRONICA COMPARTIDA DE LABORATORIO [Guía de implementación](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Laboratorio%20-%20Prototipo).
- HISTORIA CLINICA ELECTRONICA COMPARTIDA DE AYUDAS DIAGNOSTICA IMAGENOLOGIA [Guía de implementación](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Imagenolog%C3%ADa%20-%20Prototipo).
- HISTORIA CLINICA ELECTRONICA COMPARTIDA DE VACUNACION [Guía de implementación](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Vacunaci%C3%B3n%20-%20Prototipo).


[^1]: Alschuler L, Dolin R, Boyer S. HL7 Clinical Document Architecture, Release 2.0. Health Level Seven®, Inc. Secc 1.1 What is the CDA.
