# CDA Vacunación - Guía de Implementación.

* **Versión del documento:** 0.1 - Prototipo
* **Autor:** Mario Enrique Cortés M <mario.cortes@interoperar.com>
* **Compañía:** Interoperar.
* **Fecha:** 2018-08-07
* **Alcance del proyecto:** Aprendizaje.

[Ver esta guía en formato PDF](https://bitbucket.org/interoperar/hl7-skills-training-program/src/5c343a1fb7893d9de578516c237926249ba3868f/guias-implementacion/pdf/CDA-vacunacion.pdf)

## Definición:

Este documento, proporciona una guía de implementación, de la especificación HL7 CDA R2, adaptada a las necesidades de un escenario de **vacunación**, dentro del marco de un proyecto de aprendizaje, denominado **Historia Clínica Electrónica Compartida**.

El contenido ha sido redactado para facilitar la comprensión de desarrolladores o analístas, responsables de la implementación del proyecto.

El alcance de la presente guía es describir la estructura específica de un documento electrónico CDA de vacunación.  Las características generales (CDA Header y CDA Body) que comparte con los demás tipos de documentos que han sido diseñados para la *Comunidad regional de usuarios de HL7*, han sido abordados en la guía de implementación general [CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

Un documento clínico de laboratorio o CDA de vacunación es un reporte historial de inmunización o esquema de inmunización para una enfermedad, que cumple con la sintaxis, semántica y reglas de conformidad del estándar HL7 CDA R2.

Su finalidad es el intercambio electrónico de reportes de vacunación.

## Estructura general.

### Encabezado (CDA Header).

#### Nodo raíz:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Identificación unívoca del documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Código del tipo de documento:
* **Definición:** Este elemento es un tipo de dato **CE** (Coded With Equivalents) que contiene atributos de datos codificados que especifican el tipo de documento clínico.  
Por motivos de conformidad con el estándar internacional HL7 CDA R2, se recomienda respetar el uso de el nombre (displayName) en el idioma original del estándar LOINC (Inglés).
* **Nodo Xpath:** /ClinicalDocument/code
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..1
* **Tabla de codificación:** LOINC.
* **Sintaxis:**

```
<code 	code="82593-5" 
		codeSystem="2.16.840.1.113883.6.1" 
		codeSystemName="LOINC" 
		displayName="Immunization summary report"/>
```

#### Título documento:
* **Definición:** Cadena de texto (ST) donde se registra el nombre que la *Comunidad regional de usuarios de HL7* a decidido éste documento.
* **Nodo Xpath:** /ClinicalDocument/title
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplos de sintaxis:**

```
<title>HISTORIA CLINICA ELECTRONICA COMPARTIDA VACUNACION</title>
```

#### Fecha de creación del documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Código de confidencialidad de documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Código de lenguaje de documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Versión del documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Registro Médico (recordTarget):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Autor (author):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Custodio (custodian):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Autenticador legal (legalAuthenticator):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Autenticador (authenticator):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Documentación de (documentationOf):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

### Cuerpo (CDA Body).
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)


#### Sección (section):
* **Definición:** Es un componente del documento, que agrupa los registros de vacunación (entradas de datos), en una sección (Esquema de Vacunación).
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..*
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo)

#### Plantilla de la sección (templateId):
* **Definición:** Este elemento es un tipo de dato **II** (Instance Identifier) que permite el registro de la identificación de una plantilla de la estructura de la sección del documento.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/templateId
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<templateId 	root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
				extension="IMM_RMDOCSECT20180701BOG01"/>
```

#### Código del tipo de sección:
* **Definición:** Este elemento es un tipo de dato **CE** (Coded With Equivalents) qque agrupa los registros de vacunación (entradas de datos), en la sección (Esquema de Vacunación), cuyo código internacional LOINC es 41291-6 (History of Immunization).  
Por motivos de conformidad con el estándar internacional HL7 CDA R2, se recomienda respetar el uso de el nombre (displayName) en el idioma original del estándar LOINC (Inglés).
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/code
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<code	code="41291-6"
		codeSystem="2.16.840.1.113883.6.1"
		codeSystemName="LOINC"
		displayName="History of Immunization"/>
```

#### Título de la sección:
* **Definición:** Cadena de texto (ST) donde se registra el nombre que la *Comunidad regional de usuarios de HL7* a decidido para cada una de las secciones de los documentos clínicos.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/title
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<title>ESQUEMA DE VACUNACION</title>
```

#### Bloque narrativo de la sección:
* **Definición:** Este elemento es un tipo de dato **ED** (Encapsulated Data) donde se registra la información (no estructurada), del historial de vacunación, en un un formato similar a XHTML, con el fin de facilitar el procesamiento del documento, para facilitar la legibilidad humana 
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/text
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<text mediaType="text/x-hl7-text+xml">
    <table>
        <thead>
            <tr>
                <th>Vacuna</th>
                <th>Dosis</th>
                <th>Fecha</th>
                <th>Nombre comercial</th>
                <th>Lote</th>
                <th>Institución vacunadora</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>vacuna antirrábica</td>
                <td>Primera dosis</td>
                <td>20150606</td>
                <td>Imovax</td>
                <td>LT3428</td>
                <td>SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.</td>
            </tr>
            <tr>
                <td>vacuna antirrábica</td>
                <td>Segunda dosis</td>
                <td>20150613</td>
                <td>Imovax</td>
                <td>LT3478</td>
                <td>SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.</td>
            </tr>
            <tr>
                <td>vacuna antirrábica</td>
                <td>Tercera dosis</td>
                <td>20150704</td>
                <td>Imovax</td>
                <td>LT3490</td>
                <td>SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.</td>
            </tr>
        </tbody>
    </table>
</text>
```

### Entradas de datos estructurados para registros de inmunización.

De acuerdo con la especificación del estándar HL7 CDA R2 (ANSI/HL7 CDA, R2-2005), las entradas de datos estructurados (entries), representan el componente semánticamente computable de una sección del documento.  Cada sección puede contener cero o varias entradas de datos estructurados [^1].

Inmunización activa: Se realiza a través de la aplicación de vacunas, preparados antigénicos atenuados con el fin de generar una respuesta inmunitaria por parte del organismo, a fin de generar una memoria inmunitaria consistente en la formación de anticuerpos protectores contra el antígeno al que están expuestos los menores.

La inmunización activa (vacunación) es un acto de administración de una substancia química, cuya finalidad es generar una respuesta del organismo (memoria inmunitaria), propiciando la formación de anticuerpos contra un antígeno específico.

La administración de una substancia *substanceAdministration* es una clase del **RIM** [^2] , que a su vez es una extensión de la clase *procedure*. Este segmento de datos se utilza para representar procedimientos clínicos que involucran a un actor, profesional de la salud, _introduciendo o aplicando un material en un sujeto (paciente), por lo general este material es un medicamento o vacuna_.

La clase _substanceAdministration_, permite el registro de información de  administración de un medicamento o vacuna, tales como:

1. Tipo de substancia
2. Prioridad
3. Dosificación
4. periodo de "aplicacion"
5. Como aplicar el medicamento/vacuna
5. Maxima y mínima cantidad entre otros

#### Estructura:
* A continuación se definen los campos mas relevantes de la estructura de este segmento de datos asociados a la administracion de medicamentos, para mas información de este y otros componentes del CDA visite [HL7 CDA R2](http://www.hl7.org/implement/standards/product_brief.cfm?product_id=7)

  
##### Plantilla de entrada de datos (templateId):
* **Definición:** Este elemento es un tipo de dato **II** (Instance Identifier) que permite el registro de la identificación de una plantilla de la estructura de la entrada de datos.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/substanceAdministration/templateId
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<templateId	root="2.16.170.2.3.2.3.11001.1.5.3.1.2"
			extension="POCD_RMSBADM20180701BOG01"/>
```

##### Código del tipo de subtancia administrada:
* **Definición:** Código de clasificación del tipo de substancia administrada al paciente.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/substanceAdministration/code
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Tabla de codificación:** SNOMED-CT.
* **Ejemplo de sintaxis:**

```
<code 	code="398827000" 
    	codeSystem="2.16.840.1.113883.6.96" 
    	codeSystemName="SNOMED CT" 
    	displayName="vacuna"/>
```

##### Código de estado asociado a la administracion del a substancia:
* **Definición:** Código simple que indica la situación o estado del registro.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/substanceAdministration/statusCode
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Valor por defecto:** completed.
* **Ejemplo de sintaxis:**

```
	<statusCode code="completed"/>
```

##### Fecha de administración de la substancia:
* **Definición:** Fecha y hora de la administracion de la substancia.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/substanceAdministration/effectiveTime
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
	<effectiveTime value="20150704"/>
```

##### Número de la dósis administrada:
* **Definición:** Número entero que indica la dósis administrada (primera dosis, segunda dosis, etc).
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/substanceAdministration/repeatNumber
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
	<repeatNumber value="1"/>
```

##### Producto administrado (Consumible):
* **Definición:** Conjunto de datos de registro del producto administrado (vacuna).  
Este _sub elemento de la clase **substanceAdministration**_, en este contexto, nos permite relacionar la información detallada de la sustancia que se administra.
	* Código del tipo de substancia (SNOMED CT)
	* Nombre comercial
	* Número de lote de fabricación 
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/substanceAdministration/consumable/manufacturedProduct
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..1
* **Ejemplo de sintaxis:**

```
<consumable typeCode="CSM">
    <manufacturedProduct classCode="MANU">
        <manufacturedMaterial classCode="MMAT" determinerCode="KIND">
            <code code="396437008" 
                codeSystem="2.16.840.1.113883.6.96" 
                codeSystemName="SNOMED CT" 
                displayName="vacuna antirrábica"/>
            <name>Imovax</name>
            <lotNumberText>LT3428</lotNumberText>
        </manufacturedMaterial>
    </manufacturedProduct>
</consumable>
```

##### Relación con el encuentro y organización que realizó la vacunación:
* **Definición:** Conjunto de datos de registro del encuentro clínico e institución prestadora de servicios de salud donde fue aplicada la vacuna.  
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/substanceAdministration/entryRelationship/encounter
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..*
* **Ejemplo de sintaxis:**

```
<entryRelationship typeCode="COMP" contextConductionInd="true">
    <encounter classCode="ENC" moodCode="EVN">
        <performer typeCode="PRF">
            <assignedEntity classCode="ASSIGNED">
                <id root="2.16.170.11001.1000.4.128"
                    extension="1100130294"/>
                <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                    <name>
                        SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.
                    </name>
                </representedOrganization>
            </assignedEntity>
        </performer>
    </encounter>
</entryRelationship>
```

#### Ejemplo de sintaxis:

A continuación se presenta un ejemplo de una entrada de datos estructurados de vacunación.

```
<entry typeCode="COMP" contextConductionInd="true">
    <substanceAdministration classCode="SBADM" moodCode="EVN">
        <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.2"
            extension="POCD_RMSBADM20180701BOG01"/>
        <code code="398827000" 
            codeSystem="2.16.840.1.113883.6.96" 
            codeSystemName="SNOMED CT" 
            displayName="vacuna"/>
        <statusCode code="completed"/>
        <effectiveTime value="20150704"/>
        <repeatNumber value="3"/>
        <consumable typeCode="CSM">
            <manufacturedProduct classCode="MANU">
                <manufacturedMaterial classCode="MMAT" determinerCode="KIND">
                    <code code="396437008" 
                        codeSystem="2.16.840.1.113883.6.96" 
                        codeSystemName="SNOMED CT" 
                        displayName="vacuna antirrábica"/>
                    <name>Imovax</name>
                    <lotNumberText>LT3490</lotNumberText>
                </manufacturedMaterial>
            </manufacturedProduct>
        </consumable>
        <entryRelationship typeCode="COMP" contextConductionInd="true">
            <encounter classCode="ENC" moodCode="EVN">
                <performer typeCode="PRF">
                    <assignedEntity classCode="ASSIGNED">
                        <id root="2.16.170.11001.1000.4.128"
                            extension="1100130294"/>
                        <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                            <name>
                                SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.
                            </name>
                        </representedOrganization>
                    </assignedEntity>
                </performer>
            </encounter>
        </entryRelationship>
    </substanceAdministration>
</entry>
```

## Ejemplo de sintaxis:

A continuación se presenta un ejemplo de un CDA de vacunación.

[Ver archivo fuente](https://bitbucket.org/interoperar/hl7-skills-training-program/src/master/CDA-vacunacion-prototipo-1.xml)

```
<ClinicalDocument classCode="DOCCLIN" moodCode="EVN">
    <typeId root="2.16.840.1.113883.1.3" extension="POCD_HD000040"/>
    <id root="2.16.170.11001.1000.1.110013029428.20170406230458.123.1"
        extension="0f4d316b-ccce-482d-87cd-708bbe86e96a"/>
    <code code="82593-5" 
        codeSystem="2.16.840.1.113883.6.1" 
        codeSystemName="LOINC" 
        displayName="Immunization summary report"/>
    <title>HISTORIA CLINICA ELECTRONICA COMPARTIDA VACUNACION</title>
    <effectiveTime value="20150704"/>
    <confidentialityCode code="N" codeSystem="2.16.840.1.113883.5.25"
        codeSystemName="Confidentiality" displayName="normal"/>
    <languageCode code="es-CO"/>
    <versionNumber value="1"/>
    <recordTarget typeCode="RCT" contextControlCode="OP">
        <patientRole classCode="PAT">
            <id root="2.16.170.1.1.1.1.1" extension="1679884"/>
            <addr>
                <direction>Avenida Calle 68 # 34-23</direction>
                <city>Bogota D.C.</city>
                <state>Bogota D.C.</state>
                <country>Colombia</country>
            </addr>
            <telecom value="+57(300)111-1111"/>
            <telecom value="nombre@example.com"/>
            <telecom value="+57(1)2222-2222"/>
            <patient classCode="PSN" determinerCode="INSTANCE">
                <id root="2.16.170.1.1.1.1.1" extension="1679884"/>
                <name>
                    <given>Juan</given>
                    <given>Carlos</given>
                    <family>Pernia</family>
                    <family>Ayala</family>
                </name>
                <administrativeGenderCode code="M" codeSystem="2.16.840.1.113883.5.1"
                    codeSystemName="AdministrativeGender" displayName="Male"/>
                <birthTime value="19670328"/>
                <maritalStatusCode code="1" codeSystem="2.16.170.11001.1000.12.3"
                    codeSystemName="EstadoCivil" displayName="soltero"/>
            </patient>
            <providerOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.5" extension="1100130294"/>
                <name> SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E. UNIDAD DE SERVICIOS DE
                    SALUD VISTA HERMOSA </name>
            </providerOrganization>
        </patientRole>
    </recordTarget>
    <author typeCode="AUT" contextControlCode="OP">
        <time value="201704061048"/>
        <assignedAuthor>
            <id root="2.16.170.11001.1000.3.1" extension="19343111"/>
            <assignedPerson classCode="PSN" determinerCode="INSTANCE">
                <name>
                    <given>Jacinto</given>
                    <family>Carrillo</family>
                    <prefix>Dr.</prefix>
                </name>
            </assignedPerson>
            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
            </representedOrganization>
        </assignedAuthor>
    </author>
    <custodian typeCode="CST">
        <assignedCustodian classCode="ASSIGNED">
            <representedCustodianOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.5.1" extension="1100130294"
                    assigningAuthorityName="minsalud"/>
                <name> SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E. </name>
            </representedCustodianOrganization>
        </assignedCustodian>
    </custodian>
    <legalAuthenticator typeCode="LA" contextControlCode="OP">
        <time value="201704061048"/>
        <signatureCode code="S"/>
        <assignedEntity classCode="ASSIGNED">
            <id root="2.16.170.11001.1000.3.1" extension="19343111"/>
            <assignedPerson classCode="PSN" determinerCode="INSTANCE">
                <name>
                    <given>Jacinto</given>
                    <family>Carrillo</family>
                    <prefix>Dr.</prefix>
                </name>
            </assignedPerson>
            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
            </representedOrganization>
        </assignedEntity>
    </legalAuthenticator>
    <authenticator typeCode="AUTHEN">
        <time value="201704061048"/>
        <signatureCode code="S"/>
        <assignedEntity classCode="ASSIGNED">
            <id root="2.16.170.11001.1000.3.1" extension="384728374"/>
            <assignedPerson classCode="PSN" determinerCode="INSTANCE">
                <name>
                    <given>Gregorio</given>
                    <family>Casas</family>
                    <prefix>Dr.</prefix>
                </name>
            </assignedPerson>
            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
            </representedOrganization>
        </assignedEntity>
    </authenticator>
    <documentationOf typeCode="DOC">
        <serviceEvent classCode="ACT" moodCode="EVN">
            <code code="101" codeSystem="2.16.170.11001.1000.12.2"
                codeSystemName="ServiciosHabilitados" displayName="GENERAL ADULTOS"/>
        </serviceEvent>
    </documentationOf>
    <component typeCode="COMP" contextConductionInd="true">
        <structuredBody classCode="DOCBODY" moodCode="EVN">
            <component typeCode="COMP" contextConductionInd="true">
                <section classCode="DOCSECT" moodCode="EVN">
                    <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
                        extension="IMM_RMDOCSECT20180701BOG01"/>
                    <code code="41291-6" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                        displayName="History of Immunization"/>
                    <title>ESQUEMA DE VACUNACIÓN</title>
                    <text mediaType="text/x-hl7-text+xml">
                        <table>
                            <thead>
                                <tr>
                                    <th>Vacuna</th>
                                    <th>Dosis</th>
                                    <th>Fecha</th>
                                    <th>Nombre comercial</th>
                                    <th>Lote</th>
                                    <th>Institución vacunadora</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>vacuna antirrábica</td>
                                    <td>Primera dosis</td>
                                    <td>20150606</td>
                                    <td>Imovax</td>
                                    <td>LT3428</td>
                                    <td>SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.</td>
                                </tr>
                                <tr>
                                    <td>vacuna antirrábica</td>
                                    <td>Segunda dosis</td>
                                    <td>20150613</td>
                                    <td>Imovax</td>
                                    <td>LT3478</td>
                                    <td>SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.</td>
                                </tr>
                                <tr>
                                    <td>vacuna antirrábica</td>
                                    <td>Tercera dosis</td>
                                    <td>20150704</td>
                                    <td>Imovax</td>
                                    <td>LT3490</td>
                                    <td>SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.</td>
                                </tr>
                            </tbody>
                        </table>
                    </text>
                    <entry typeCode="COMP" contextConductionInd="true">
                        <substanceAdministration classCode="SBADM" moodCode="EVN">
                            <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.2"
                                extension="POCD_RMSBADM20180701BOG01"/>
                            <code code="398827000" 
                                codeSystem="2.16.840.1.113883.6.96" 
                                codeSystemName="SNOMED CT" 
                                displayName="vacuna"/>
                            <statusCode code="completed"/>
                            <effectiveTime value="20150606"/>
                            <repeatNumber value="1"/>
                            <consumable typeCode="CSM">
                                <manufacturedProduct classCode="MANU">
                                    <manufacturedMaterial classCode="MMAT" determinerCode="KIND">
                                        <code code="396437008" 
                                            codeSystem="2.16.840.1.113883.6.96" 
                                            codeSystemName="SNOMED CT" 
                                            displayName="vacuna antirrábica"/>
                                        <name>Imovax</name>
                                        <lotNumberText>LT3428</lotNumberText>
                                    </manufacturedMaterial>
                                </manufacturedProduct>
                            </consumable>
                            <entryRelationship typeCode="COMP" contextConductionInd="true">
                                <encounter classCode="ENC" moodCode="EVN">
                                    <performer typeCode="PRF">
                                        <assignedEntity classCode="ASSIGNED">
                                            <id root="2.16.170.11001.1000.4.128"
                                                extension="1100130294"/>
                                            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                                                <name>
                                                    SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.
                                                </name>
                                            </representedOrganization>
                                        </assignedEntity>
                                    </performer>
                                </encounter>
                            </entryRelationship>
                        </substanceAdministration>
                    </entry>
                    <entry typeCode="COMP" contextConductionInd="true">
                        <substanceAdministration classCode="SBADM" moodCode="EVN">
                            <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.2"
                                extension="POCD_RMSBADM20180701BOG01"/>
                            <code code="398827000" 
                                codeSystem="2.16.840.1.113883.6.96" 
                                codeSystemName="SNOMED CT" 
                                displayName="vacuna"/>
                            <statusCode code="completed"/>
                            <effectiveTime value="20150613"/>
                            <repeatNumber value="2"/>
                            <consumable typeCode="CSM">
                                <manufacturedProduct classCode="MANU">
                                    <manufacturedMaterial classCode="MMAT" determinerCode="KIND">
                                        <code code="396437008" 
                                            codeSystem="2.16.840.1.113883.6.96" 
                                            codeSystemName="SNOMED CT" 
                                            displayName="vacuna antirrábica"/>
                                        <name>Imovax</name>
                                        <lotNumberText>LT3478</lotNumberText>
                                    </manufacturedMaterial>
                                </manufacturedProduct>
                            </consumable>
                            <entryRelationship typeCode="COMP" contextConductionInd="true">
                                <encounter classCode="ENC" moodCode="EVN">
                                    <performer typeCode="PRF">
                                        <assignedEntity classCode="ASSIGNED">
                                            <id root="2.16.170.11001.1000.4.128"
                                                extension="1100130294"/>
                                            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                                                <name>
                                                    SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.
                                                </name>
                                            </representedOrganization>
                                        </assignedEntity>
                                    </performer>
                                </encounter>
                            </entryRelationship>
                        </substanceAdministration>
                    </entry>
                    <entry typeCode="COMP" contextConductionInd="true">
                        <substanceAdministration classCode="SBADM" moodCode="EVN">
                            <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.2"
                                extension="POCD_RMSBADM20180701BOG01"/>
                            <code code="398827000" 
                                codeSystem="2.16.840.1.113883.6.96" 
                                codeSystemName="SNOMED CT" 
                                displayName="vacuna"/>
                            <statusCode code="completed"/>
                            <effectiveTime value="20150704"/>
                            <repeatNumber value="3"/>
                            <consumable typeCode="CSM">
                                <manufacturedProduct classCode="MANU">
                                    <manufacturedMaterial classCode="MMAT" determinerCode="KIND">
                                        <code code="396437008" 
                                            codeSystem="2.16.840.1.113883.6.96" 
                                            codeSystemName="SNOMED CT" 
                                            displayName="vacuna antirrábica"/>
                                        <name>Imovax</name>
                                        <lotNumberText>LT3490</lotNumberText>
                                    </manufacturedMaterial>
                                </manufacturedProduct>
                            </consumable>
                            <entryRelationship typeCode="COMP" contextConductionInd="true">
                                <encounter classCode="ENC" moodCode="EVN">
                                    <performer typeCode="PRF">
                                        <assignedEntity classCode="ASSIGNED">
                                            <id root="2.16.170.11001.1000.4.128"
                                                extension="1100130294"/>
                                            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                                                <name>
                                                    SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.
                                                </name>
                                            </representedOrganization>
                                        </assignedEntity>
                                    </performer>
                                </encounter>
                            </entryRelationship>
                        </substanceAdministration>
                    </entry>
                </section>
            </component>
        </structuredBody>
    </component>
</ClinicalDocument>
```


[^1]: Alschuler L, Dolin R, Boyer S. HL7 Clinical Document Architecture, Release 2.0. Health Level Seven®, Inc. Secc 4.3.6  Entry Acts.
[^2]: Health Level Seven®, Inc. HL7 Reference Information Model.