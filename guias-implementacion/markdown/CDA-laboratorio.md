# CDA Laboratorio clínico - Guía de Implementación.

* **Versión del documento:** 0.1 - Prototipo
* **Autor:** Mario Enrique Cortés M <mario.cortes@interoperar.com>
* **Compañía:** Interoperar.
* **Fecha:** 2018-08-07
* **Alcance del proyecto:** Aprendizaje.

[Ver esta guía en formato PDF](https://bitbucket.org/interoperar/hl7-skills-training-program/src/5c343a1fb7893d9de578516c237926249ba3868f/guias-implementacion/pdf/CDA-laboratorio.pdf)

## Definición:

Este documento, proporciona una guía de implementación, de la especificación HL7 CDA R2, adaptada a las necesidades de un escenario de **laboratorio clínico**, dentro del marco de un proyecto de aprendizaje, denominado **Historia Clínica Electrónica Compartida**

El contenido ha sido redactado para facilitar la comprensión de desarrolladores o analístas, responsables de la implementación del proyecto.

El alcance de la presente guía es describir la estructura específica de un documento electrónico CDA de laboratorio clínico.  Las características generales (CDA Header y CDA Body) que comparte con los demás tipos de documentos que han sido diseñados por la *Comunidad regional de usuarios de HL7*, han sido abordados en la guía de implementación general [CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

Un documento clínico de laboratorio o CDA de laboratorio es una reporte de pruebas diagnósticas, que cumple con la sintaxis, semántica y reglas de conformidad del estándar HL7 CDA R2.

Su finalidad es el intercambio electrónico de reportes diagnósticos de laboratorio clínico.

## Estructura general.

### Encabezado (CDA Header).

#### Nodo raíz:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Identificación unívoca del documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Código del tipo de documento:
* **Definición:** Este elemento es un tipo de dato **CE** (Coded With Equivalents) que contiene atributos de datos codificados que especifican el tipo de documento clínico.  
Por motivos de conformidad con el estándar internacional HL7 CDA R2, se recomienda respetar el uso de el nombre (displayName) en el idioma original del estándar LOINC (Inglés).
* **Nodo Xpath:** /ClinicalDocument/code
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..1
* **Tabla de codificación:** LOINC.
* **Sintaxis:**

```
<code	code="11502-2" 
		codeSystem="2.16.840.1.113883.6.1" 
		codeSystemName="LOINC" 
		displayName="Laboratory report"/>
```

#### Título documento:
* **Definición:** Cadena de texto (ST) donde se registra el nombre que la *Comunidad regional de usuarios de HL7* a decidido éste documento.
* **Nodo Xpath:** /ClinicalDocument/title
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplos de sintaxis:**

```
<title>HISTORIA CLINICA ELECTRONICA COMPARTIDA LABORATORIO</title>
```

#### Fecha de creación del documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Código de confidencialidad de documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Código de lenguaje de documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Versión del documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Registro Médico (recordTarget):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Autor (author):
* **Definición:** Es un elemento que permite registrar información de la(s) persona(s) que participa(n) como autor(es) del documento clínico (Bacteriólogos).  

[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Custodio (custodian):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Autenticador legal (legalAuthenticator):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Autenticador (authenticator):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Documentación de (documentationOf):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

### Cuerpo (CDA Body).
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).


#### Sección (section):
* **Definición:** Es un componente del documento, que agrupa los resultados de las pruebas diagnósticas de laboratorio (entradas de datos), en secciones (Química, Hematología, Microbiología, etc).
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..*
* **Ejemplo de sintaxis:**

```
<component typeCode="COMP" contextConductionInd="true">
    <structuredBody classCode="DOCBODY" moodCode="EVN">
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- SECCIÓN DE PRUEBAS DIAGNÓSTICAS DE QUÍMICA -->
                ...
            </section>
        </component>
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- SECCIÓN DE PRUEBAS DIAGNÓSTICAS DE HEMATOLOGÍA -->
                ...
            </section>
        </component>
        ...
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- SECCIÓN DE PRUEBAS DIAGNÓSTICAS DE CITOLOGÍA -->
                ...
            </section>
        </component>
    </structuredBody>
</component>
```

#### Plantilla de la sección (templateId):
* **Definición:** Este elemento es un tipo de dato **II** (Instance Identifier) que permite el registro de la identificación de una plantilla de la estructura de la sección del documento.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/templateId
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<templateId	root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
    		extension="POLB_RMDOCSECT20180701BOG01"/>
```

#### Código del tipo de sección:
* **Definición:** Este elemento es un tipo de dato **CE** (Coded With Equivalents) que contiene atributos de datos codificados que especifican el tipo de sección que agrupa los resultados de las pruebas diagnósticas que hacen parte del documento clínico de laboratorio.
Por motivos de conformidad con el estándar internacional HL7 CDA R2, se recomienda respetar el uso de el nombre (displayName) en el idioma original del estándar LOINC (Inglés).
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/code
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Tabla de codificación:** LOINC.

CÓDIGO (LOINC)|SECCIÓN DE LABORATORIO|TÍTULO
:------------:|----------------------|------
26436-6|Laboratory studies|PRUEBAS DIAGNÓSTICAS DE LABORATORIO
18719-5|Chemistry studies|PRUEBAS DIAGNÓSTICAS DE QUÍMICA
18767-4|Blood gas studies|PRUEBAS DIAGNÓSTICAS DE GASES SANGUÍNEOS
18723-7|Hematology studies|PRUEBAS DIAGNÓSTICAS DE HEMATOLOGÍA
18725-2|Microbiology studies|PRUEBAS DIAGNÓSTICAS DE MICROBIOLOGÍA
18727-8|Serology studies|PRUEBAS DIAGNÓSTICAS DE SEROLOGÍA
18728-6|Toxicology studies|PRUEBAS DIAGNÓSTICAS DE TOXICOLOGÍA
18729-4|Urinalysis studies|PRUEBAS DIAGNÓSTICAS DE UROANÁLISIS
18717-9|Blood bank studies|PRUEBAS DIAGNÓSTICAS DE BANCO DE SANGRE18718-7|Cell marker studies|PRUEBAS DIAGNÓSTICAS DE MARCADORES CELULARES18720-3|Coagulation studies|PRUEBAS DIAGNÓSTICAS DE COAGULACIÓN18721-1|Therapeutic drug monitoring studies|PRUEBAS DIAGNÓSTICAS DE MONITORÉO DE MEDICAMENTOS TERAPÉUTICOS18722-9|Fertility studies|PRUEBAS DIAGNÓSTICAS DE FERTILIDAD18724-5|HLA studies|PRUEBAS DIAGNÓSTICAS DE ANTÍGENO LEUCOCITARIO HUMANO18768-2|Cell counts+​Differential studies|PRUEBAS DIAGNÓSTICAS DE HEMOGRAMA18769-0|Microbial susceptibility tests|PRUEBAS DIAGNÓSTICAS DE SUSCEPTIBILIDAD MICROBIANA26435-8|Molecular pathology studies|PRUEBAS DIAGNÓSTICAS DE PATOLOGÍA MOLECULAR26437-4|Chemistry challenge studies|PRUEBAS DIAGNÓSTICAS DE PRUEBAS ESPECIALES DE BIOQUÍMICA26438-2|Cytology studies|PRUEBAS DIAGNÓSTICAS DE CITOLOGÍA

* **Ejemplos de sintaxis:**

```
<code	code="18725-2" 
		codeSystem="2.16.840.1.113883.6.1" 
		codeSystemName="LOINC" 
		displayName="Microbiology studies"/>
```


#### Título de la sección:
* **Definición:** Cadena de texto (ST) donde se registra el nombre que la *Comunidad regional de usuarios de HL7* a decidido para cada una de las secciones de los documentos clínicos.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/title
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<title>PRUEBAS DIAGNÓSTICAS DE HEMOGRAMA</title>
```

#### Bloque narrativo de la sección:
* **Definición:** Este elemento es un tipo de dato **ED** (Encapsulated Data) donde se registra la información (no estructurada), de los resultados de pruebas diagnósticas de laboratorio, en un un formato similar a XHTML, con el fin de facilitar el procesamiento del documento, para facilitar la legibilidad humana 
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/text
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<text mediaType="text/x-hl7-text+xml">
    <table>
        <thead>
            <tr>
                <th>Código</th>
                <th>Nombre de la prueba</th>
                <th>Valor</th>
                <th>Unidades</th>
                <th>Rango de referencia</th>
                <th>Interpretación</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>903426</td>
                <td>HEMOGLOBINA GLICOSILADA AUTOMATIZADA</td>
                <td>5.3</td>
                <td>%</td>
                <td>4.0% - 5.6%</td>
                <td>Normal</td>
            </tr>
        </tbody>
    </table>
</text>
```

### Entradas de datos estructurados para resultados diagnósticos de laboratorio clínico.

De acuerdo con la especificación del estándar HL7 CDA R2 (ANSI/HL7 CDA, R2-2005), las entradas de datos estructurados (entries), representan el componente semánticamente computable de una sección del documento.  Cada sección puede contener cero o varias entradas de datos estructurados [^1].

Las observaciones son uno de los actos principales en las actividades asistenciales. Son utilizadas para soporte diagnóstico, monitoreo de progreso, determinar patrones y otras impresiones diagnósticas de los profesionales de la salud.

De acuerdo con el estándar ANSI/HL7 V3 RIM, R1-2003 (Reference Information Model), las observaciones (observations) permiten el registro de actos que incluyen, entre otros, resultados de laboratorio clínico [^2]. 

Desde el punto de vista de las estructuras de datos, existen dos (2) tipos principales de agrupaciones de resultados diagnósticos de laboratorio clínico: 

1. Resultados de pruebas
2. Resultados de paneles de pruebas

#### Entradas de resultados de pruebas diagnósticas de laboratorio.

##### Definición:
Una prueba es una operación realizada en un laboratorio o un punto de atención, por medios manuales, instrumentales, sistematizados o automatizados (dispositivo analizador), para generar una o más **observaciones** (resultados). Dichas observaciones pueden ser obtenidas a través de la medición de una cantidad de espécimen (muestra) in- vitro, hallazgos en el espécimen, cálculos realizados a partir de otras observaciones o datos, o cualquier otro medio [^3].

Son observaciones simples, cuya estructura incluye un código de identificación del tipo de prueba, el valor del resultados y un conjunto de metadatos.

##### Estructura:

###### Código de la prueba (observación):
* **Definición:** Datos codificados que especifican tipo de prueba (observación) realizada.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/code
* **Requerimiento:** Mandatorio.
* **Tabla de codificación:** CUPS.
* **Ejemplo de sintaxis:**

	```
		<code	code="903426" 
				codeSystem="2.16.170.11001.1000.14" 
				codeSystemName="CUPS" 
				displayName="HEMOGLOBINA GLICOSILADA AUTOMATIZADA"/>
	```

###### Código de estatus de la prueba (observación): 
* **Definición:** Código simple que indica la situación o estado del registro.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/statusCode
* **Requerimiento:** Mandatorio.
* **Valor por defecto:** completed.
* **Ejemplo de sintaxis:**

	```
		<statusCode code="completed"/>
	```

###### Fecha de la prueba (observación): 
* **Definición:** Fecha y hora de generación del resultado.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/effectiveTime
* **Requerimiento:** Mandatorio.
* **Ejemplo de sintaxis:**

	```
		<effectiveTime value="20180802165000"/>
	```

###### Valor del restulado de la prueba (observación): 
* **Definición:** valor del resultado.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/value
* **Requerimiento:** Mandatorio.
* **Ejemplos de sintaxis:**
	* **Resultado tipo cantidad física:**

		```
			<value xsi:type="PQ" value="34" unit="mmol/mol" />
		```
		
	* **Resultado tipo concepto codificado:**

		```
			<value  xsi:type="CD" code="10828004" 
          			codeSystem="2.16.840.1.113883.6.96" 
       				codeSystemName="SNOMED CT"
       				displayName="positivo"/>
		```
		
	* **Resultado tipo cadena de texto:**

		```
			<value xsi:type="ST">
				se observa piocitos presentes en esputo
			</value>
		```

###### Interpretación del resultado de la prueba (observación): 
* **Definición:** Datos codificados de interpretación de la observación de resultado.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/interpretationCode
* **Requerimiento:** Opcional.
* **Tabla de codificación:** ObservationInterpretation (HL7 v3).
* **Ejemplos de sintaxis:**
	* **Interpretación normal:**

		```
			<interpretationCode code="N"
								codeSystem="2.16.840.1.113883.5.83"
								codeSystemName="ObservationInterpretation" 
								displayName="Normal"/>
		```
		
	* **Interpretación baja:**

		```
			<interpretationCode	code="L"
								codeSystem="2.16.840.1.113883.5.83"
								codeSystemName="ObservationInterpretation"
								displayName="Low"/>
		```
		
	* **Interpretación alta:**
	
		```
			<interpretationCode	code="H"
								codeSystem="2.16.840.1.113883.5.83"
								codeSystemName="ObservationInterpretation"
								displayName="High"/>
		```

###### Rango de referencia del resultado de la prueba (observación): 
* **Definición:** Datos de referencia que permiten la interpretación la observación de resultado.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/referenceRange
* **Requerimiento:** Opcional.
* **Ejemplo de sintaxis:**
	
	```
		<referenceRange typeCode="REFV">
			<observationRange classCode="OBS" moodCode="EVN.CRT">
				<value xsi:type="ST">4.0% - 5.6%</value>
			</observationRange>
		</referenceRange>
	```

##### Ejemplo de sintaxis:

A continuación se presenta un ejemplo de una entrada de datos estructurados de resultados de una observación o prueba diagnóstica de laboratorio clínico.

```
<entry typeCode="COMP">
		<observation classCode="OBS" moodCode="EVN">
			<code 	code="903426" 
					codeSystem="2.16.170.11001.1000.14" 
					codeSystemName="CUPS" 
					displayName="HEMOGLOBINA GLICOSILADA AUTOMATIZADA"/>
			<statusCode code="completed"/>
			<effectiveTime value="20180802165000"/>
			<value xsi:type="PQ" value="5.3" unit="%" />
			<interpretationCode	code="N" 
								codeSystem="2.16.840.1.113883.5.83" 
								codeSystemName="ObservationInterpretation" 
								displayName="Normal"/>
			<referenceRange typeCode="REFV">
				<observationRange classCode="OBS" moodCode="EVN.CRT">
					<value xsi:type="ST">4.0% - 5.6%</value>
				</observationRange>
			</referenceRange>
		</observation>
</entry>
```


#### Entradas de resultados de paneles de pruebas diagnósticas de laboratorio.

##### Definición:
Un conjunto de una o varias pruebas (observaciones) de laboratorio (analitos), identificados por un único nombre y código [^4].

Son observaciones complejas, cuya estructura incluye un código de identificación del tipo de prueba, un conjunto de metadatos y múltiples observaciones asociadas (ej: hemograma, pefil lipídico, uroanálisis, antibiograma, etc.).

##### Estructura:

###### Código del tipo panel de pruebas diagnósticas (observación):
* **Definición:** Datos codificados que especifical tipo de panel de pruebas diagnósticas (observación) realizado.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/code
* **Requerimiento:** Mandatorio.
* **Tabla de codificación:** CUPS.
* **Ejemplo de sintaxis:**

	```
		<code	code="902210" 
				codeSystem="2.16.170.11001.1000.14" 
				codeSystemName="CUPS" 
				displayName="HEMOGRAMA IV (HEMOGLOBINA HEMATOCRITO RECUENTO DE ERITROCITOS ÍNDICES ERITROCITARIOS LEUCOGRAMA RECUENTO DE PLAQUETAS ÍNDICES PLAQUETARIOS Y MORFOLOGiA ELECTRÓNICA E HISTOGRAMA) AUTOMATIZADO"/>
	```


###### Fecha del panel de pruebas diagnósticas (observación): 
* **Definición:** Fecha y hora de generación de los resultados del panel de pruebas diagnósticas.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/effectiveTime
* **Requerimiento:** Mandatorio.
* **Ejemplo de sintaxis:**

	```
		<effectiveTime value="20180802165000"/>
	```

###### Componentes del panel de pruebas diagnósticas: 
* **Definición:** Elementos recurrentes dentro de la estructura, que permiten el registro de múltiples observaciones (analitos) que componen el panel de pruebas diagnósticas (battery).
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/entryRelationship/organizer
* **Requerimiento:** Mandatorio.
* **Ejemplo de sintaxis:**

	```
		<entry typeCode="COMP">
		    <observation classCode="OBS" moodCode="EVN">
		        ...
		        <entryRelationship typeCode="COMP">
		            <organizer classCode="BATTERY" moodCode="EVN">
		                <statusCode code="completed"/>
		                <component typeCode="COMP">
		                    ...
		                </component>
		                ...
		                <component>
		                    ...
		                </component>
		            </organizer>
		        </entryRelationship>
		    </observation>
		</entry>
	```

###### Código de estatus del panel de pruebas diagnósticas (componentes observaciones relacionada): 
* **Definición:** Código simple que indica la situación o estado del registro de los componentes (observaciones) que hacen parte del panel de pruebas diagnósticas.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/entryRelationship/organizer/statusCode
* **Requerimiento:** Mandatorio.
* **Valor por defecto:** completed.
* **Ejemplo de sintaxis:**

	```
		<statusCode code="completed"/>
	```

###### Analito (observación relacionada):
* **Definición:** Cada una de las observaciones relacionadas que hacen parte del panel de prueba diagnóstica (battery).
Se trata de un elemento recurrente [1..*] dentro de la estructura de entrada de datos estructurados del panel de prueba diagnóstica.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/entryRelationship/organizer/component/observation
* **Requerimiento:** Mandatorio.
* **Ejemplo de sintaxis:**

	```
		<entry typeCode="COMP">
		    <observation classCode="OBS" moodCode="EVN">
		        ...
		        <entryRelationship typeCode="COMP">
		            <organizer classCode="BATTERY" moodCode="EVN">
		                <statusCode code="completed"/>
		                <component typeCode="COMP">
		                    <observation classCode="OBS" moodCode="EVN">
		                        ...
		                    </observation>
		                </component>
		                ...
		                <component>
		                    <observation classCode="OBS" moodCode="EVN">
		                        ...
		                    </observation>
		                </component>
		            </organizer>
		        </entryRelationship>
		    </observation>
		</entry>
	```

###### Código del analito (observación relacionada):
* **Definición:** Datos codificados que especifical tipo de analito.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/entryRelationship/observation/code
* **Requerimiento:** Mandatorio.
* **Tabla de codificación:** LOINC.
* **Ejemplo de sintaxis:**
	
	```
		<code	code="6690-2" 
				codeSystem="2.16.840.1.113883.6.1" 
				codeSystemName="LOINC" 
				displayName="Leukocytes"/>
	```

###### Valor del restulado del analito (observación relacionada): 
* **Definición:** valor de resultado del analito.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/entryRelationship/observation/value
* **Requerimiento:** Mandatorio.
* **Ejemplos de sintaxis:**
	* **Resultado tipo cantidad física:**

		```
			<value xsi:type="PQ" value="8.88" unit="10^3/uL" />
		```
		
	* **Resultado tipo concepto codificado:**

		```
			<value  xsi:type="CD" code="83185005" 
          			codeSystem="2.16.840.1.113883.6.96" 
       				codeSystemName="SNOMED CT"
       				displayName="sensible"/>
		```
		
	* **Resultado tipo cadena de texto:**

		```
			<value xsi:type="ST">
				se observa sedimento biliar en orina
			</value>
		```

###### Interpretación del resultado del analito (observación relacionada): 
* **Definición:** Datos codificados de interpretación de la observación de resultado del analito.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/entryRelationship/observation/interpretationCode
* **Requerimiento:** Opcional.
* **Tabla de codificación:** ObservationInterpretation (HL7 v3).
* **Ejemplos de sintaxis:**
	* **Interpretación normal:**

		```
			<interpretationCode code="N"
								codeSystem="2.16.840.1.113883.5.83"
								codeSystemName="ObservationInterpretation" 
								displayName="Normal"/>
		```
		
	* **Interpretación baja:**

		```
			<interpretationCode	code="L"
								codeSystem="2.16.840.1.113883.5.83"
								codeSystemName="ObservationInterpretation"
								displayName="Low"/>
		```
		
	* **Interpretación alta:**
	
		```
			<interpretationCode	code="H"
								codeSystem="2.16.840.1.113883.5.83"
								codeSystemName="ObservationInterpretation"
								displayName="High"/>
		```

###### Rango de referencia del resultado del analito (observación relacionada): 
* **Definición:** Datos de referencia que permiten la interpretación la observación de resultado del analito.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/entryRelationship/observation/referenceRange
* **Requerimiento:** Opcional.
* **Ejemplo de sintaxis:**
	
	```
		<referenceRange typeCode="REFV">
			<observationRange classCode="OBS" moodCode="EVN.CRT">
				<value xsi:type="ST">5 - 10</value>
			</observationRange>
		</referenceRange>
	```

###### Información multimedia (observación relacionada):
* **Definición:** Observaciones tipo multimedia (generalmente gráficas o imágenes) que hacen parte del panel de prueba diagnóstica (battery).
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/observation/entryRelationship/organizer/component/observationMedia
* **Requerimiento:** Mandatorio.
* **Ejemplo de sintaxis:**
	* **Imagen referenciada en una URL:**

	```
		<entry typeCode="COMP">
    		<observation classCode="OBS" moodCode="EVN">
        		...
        		<entryRelationship typeCode="COMP">
            		<organizer classCode="BATTERY" moodCode="EVN">
                		<statusCode code="completed"/>
                		<component typeCode="COMP">
                    	...
                		</component>
                		...
                		<component>
                    	<observationMedia classCode="OBS" moodCode="EVN">
                        	<value mediaType="image/jpeg">
                            	<reference value="https://server.example.com/images/image.jpeg"/>
                        	</value>
                    	</observationMedia>
                		</component>
            		</organizer>
        		</entryRelationship>
    		</observation>
		</entry>
	```

	* **Imagen encapsulada dentro del mensaje (codificación base64):**
	
	```
		<observationMedia classCode="OBS" moodCode="EVN">
			<value representation="B64" mediaType="image/jpeg">
				/9j/4Q/xRXhpZgAATU0AKgAAAAgA......tUvh5/Dz+AU /wCb9n4B8D4IfD//2Q==.
			</value>
		</observationMedia>
	```

##### Ejemplo de sintaxis:

A continuación se presenta un ejemplo de una entrada de datos estructurados de resultados de un panel de pruebas diagnósticas (ej: hemograma).

```
<entry typeCode="COMP">
    <observation classCode="OBS" moodCode="EVN">
        <code code="902210" codeSystem="2.16.170.11001.1000.14" codeSystemName="CUPS"
            displayName="HEMOGRAMA IV (HEMOGLOBINA HEMATOCRITO RECUENTO DE ERITROCITOS ÍNDICES ERITROCITARIOS LEUCOGRAMA RECUENTO DE PLAQUETAS ÍNDICES PLAQUETARIOS Y MORFOLOGiA ELECTRÓNICA E HISTOGRAMA) AUTOMATIZADO"/>
        <statusCode code="completed"/>
        <effectiveTime value="20180802165000"/>
        <entryRelationship typeCode="COMP">
            <organizer classCode="BATTERY" moodCode="EVN">
                <statusCode code="completed"/>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.1"/>
                        <code code="6690-2" codeSystem="2.16.840.1.113883.6.1"
                            codeSystemName="LOINC" displayName="Leukocytes"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="8.88" unit="10^3/uL"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">5 - 10</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.2"/>
                        <code code="789-8" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Erythrocytes"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="4.52" unit="10^6/uL"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">4-5.5</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.3"/>
                        <code code="777-3" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Platelets"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="454" unit="10^3/uL"/>
                        <interpretationCode code="H" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="High"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">150-400</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.4"/>
                        <code code="718-7" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Hemoglobin"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="12.1" unit="g/dL"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">12-17.4</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.5"/>
                        <code code="731-0" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Lymphocytes"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="2.86" unit="10^3/uL"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">1.3-4</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.6"/>
                        <code code="742-7" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Monocytes"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="0.42" unit="10^3/uL"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">0.15-0.7</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.7"/>
                        <code code="751-8" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Neutrophils"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="5.41" unit="10^3/uL"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">2-7.5</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.8"/>
                        <code code="711-2" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Eosinophils"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="0.14" unit="10^3/uL"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">0-0.15</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.9"/>
                        <code code="704-7" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Basophils"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="0.05" unit="10^3/uL"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">0-0.15</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.10"/>
                        <code code="736-9" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Lymphocytes/100 leukocytes"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="32.2" unit="%"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">21-40</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.11"/>
                        <code code="5905-5" codeSystem="2.16.840.1.113883.6.1"
                            codeSystemName="LOINC" displayName="Monocytes/100 leukocytes "/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="4.7" unit="%"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">3-7</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.12"/>
                        <code code="770-8" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Neutrophils/100 leukocytes"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="60.9" unit="%"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">40-75</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.13"/>
                        <code code="713-8" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Eosinophils/100 leukocytes"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="1.6" unit="%"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">0-5</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.14"/>
                        <code code="706-2" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Basophils/100 leukocytes"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="0.6" unit="%"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">0-1.5</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.15"/>
                        <code code="4544-3" codeSystem="2.16.840.1.113883.6.1"
                            codeSystemName="LOINC" displayName="Hematocrit"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="38.1" unit="%"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">36-52</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.16"/>
                        <code code="787-2" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Erythrocyte mean corpuscular volume "/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="84.2" unit="fL"/>
                        <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Normal"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">76-96</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component typeCode="COMP">
                    <observation classCode="OBS" moodCode="EVN">
                        <id extension="OBX.17"/>
                        <code code="785-6" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                            displayName="Erythrocyte mean corpuscular hemoglobin"/>
                        <effectiveTime value="20180802165000"/>
                        <value xsi:type="PQ" value="26.8" unit="pg"/>
                        <interpretationCode code="L" codeSystem="2.16.840.1.113883.5.83"
                            codeSystemName="ObservationInterpretation" displayName="Low"/>
                        <referenceRange typeCode="REFV">
                            <observationRange classCode="OBS" moodCode="EVN.CRT">
                                <value xsi:type="ST">27-32</value>
                            </observationRange>
                        </referenceRange>
                    </observation>
                </component>
                <component>
                    <observationMedia classCode="OBS" moodCode="EVN">
                        <id extension="OBX.18"/>
                        <value representation="B64" mediaType="image/jpeg">
                            /9j/4Q/xRXhpZgAATU0AKgAAAAgA......tUvh5/Dz+AU /wCb9n4B8D4IfD//2Q==.
                        </value>
                    </observationMedia>
                </component>
            </organizer>
        </entryRelationship>
    </observation>
</entry>
```

## Ejemplo de sintaxis:

A continuación se presenta un ejemplo de un CDA de laboratorio clínico.

[Ver archivo fuente](https://bitbucket.org/interoperar/hl7-skills-training-program/src/master/CDA-laboratorio-prototipo-1.xml)

```
<ClinicalDocument classCode="DOCCLIN" moodCode="EVN">
    <typeId root="2.16.840.1.113883.1.3" extension="POCD_HD000040"/>
    <id root="2.16.170.11001.1000.1.110013029428.20170406230458.123.1"
        extension="f0eb36f1-d79d-4bde-8de8-c76151755c41"/>
    <code code="11502-2" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
        displayName="Laboratory report"/>
    <title>HISTORIA CLINICA ELECTRONICA COMPARTIDA LABORATORIO</title>
    <effectiveTime value="201704061048"/>
    <confidentialityCode code="N" codeSystem="2.16.840.1.113883.5.25"
        codeSystemName="Confidentiality" displayName="normal"/>
    <languageCode code="es-CO"/>
    <versionNumber value="1"/>
    <recordTarget typeCode="RCT" contextControlCode="OP">
        <patientRole classCode="PAT">
            <id root="2.16.170.1.1.1.1.1" extension="1679884"/>
            <addr>
                <direction>Avenida Calle 68 # 34-23</direction>
                <city>Bogota D.C.</city>
                <state>Bogota D.C.</state>
                <country>Colombia</country>
            </addr>
            <telecom value="+57(300)111-1111"/>
            <telecom value="nombre@example.com"/>
            <telecom value="+57(1)2222-2222"/>
            <patient classCode="PSN" determinerCode="INSTANCE">
                <id root="2.16.170.1.1.1.1.1" extension="1679884"/>
                <name>
                    <given>Juan</given>
                    <given>Carlos</given>
                    <family>Pernia</family>
                    <family>Ayala</family>
                </name>
                <administrativeGenderCode code="M" codeSystem="2.16.840.1.113883.5.1"
                    codeSystemName="AdministrativeGender" displayName="Male"/>
                <birthTime value="19670328"/>
                <maritalStatusCode code="1" codeSystem="2.16.170.11001.1000.12.3"
                    codeSystemName="EstadoCivil" displayName="soltero"/>
            </patient>
            <providerOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.5" extension="1100130294"/>
                <name> SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E. UNIDAD DE SERVICIOS DE
                    SALUD VISTA HERMOSA </name>
            </providerOrganization>
        </patientRole>
    </recordTarget>
    <author typeCode="AUT" contextControlCode="OP">
        <time value="201704061048"/>
        <assignedAuthor>
            <id root="2.16.170.11001.1000.3.1" extension="19343111"/>
            <assignedPerson classCode="PSN" determinerCode="INSTANCE">
                <name>
                    <given>Jacinto</given>
                    <family>Carrillo</family>
                    <prefix>Dr.</prefix>
                </name>
            </assignedPerson>
            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
            </representedOrganization>
        </assignedAuthor>
    </author>
    <custodian typeCode="CST">
        <assignedCustodian classCode="ASSIGNED">
            <representedCustodianOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.5.1" extension="1100130294"
                    assigningAuthorityName="minsalud"/>
                <name> SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E. </name>
            </representedCustodianOrganization>
        </assignedCustodian>
    </custodian>
    <legalAuthenticator typeCode="LA" contextControlCode="OP">
        <time value="201704061048"/>
        <signatureCode code="S"/>
        <assignedEntity classCode="ASSIGNED">
            <id root="2.16.170.11001.1000.3.1" extension="19343111"/>
            <assignedPerson classCode="PSN" determinerCode="INSTANCE">
                <name>
                    <given>Jacinto</given>
                    <family>Carrillo</family>
                    <prefix>Dr.</prefix>
                </name>
            </assignedPerson>
            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
            </representedOrganization>
        </assignedEntity>
    </legalAuthenticator>
    <authenticator typeCode="AUTHEN">
        <time value="201704061048"/>
        <signatureCode code="S"/>
        <assignedEntity classCode="ASSIGNED">
            <id root="2.16.170.11001.1000.3.1" extension="384728374"/>
            <assignedPerson classCode="PSN" determinerCode="INSTANCE">
                <name>
                    <given>Gregorio</given>
                    <family>Casas</family>
                    <prefix>Dr.</prefix>
                </name>
            </assignedPerson>
            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
            </representedOrganization>
        </assignedEntity>
    </authenticator>
    <documentationOf typeCode="DOC">
        <serviceEvent classCode="ACT" moodCode="EVN">
            <code code="101" codeSystem="2.16.170.11001.1000.12.2"
                codeSystemName="ServiciosHabilitados" displayName="GENERAL ADULTOS"/>
        </serviceEvent>
    </documentationOf>
    <component typeCode="COMP" contextConductionInd="true">
        <structuredBody classCode="DOCBODY" moodCode="EVN">
            <component typeCode="COMP" contextConductionInd="true">
                <section classCode="DOCSECT" moodCode="EVN">
                    <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
                        extension="POLB_RMDOCSECT20180701BOG01"/>
                    <code code="18719-5" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                        displayName="Chemistry studies"/>
                    <title>PRUEBAS DIAGNÓSTICAS DE QUÍMICA</title>
                    <text mediaType="text/x-hl7-text+xml">
                        <table>
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre de la prueba</th>
                                    <th>Valor</th>
                                    <th>Unidades</th>
                                    <th>Rango de referencia</th>
                                    <th>Interpretación</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>903426</td>
                                    <td>HEMOGLOBINA GLICOSILADA AUTOMATIZADA</td>
                                    <td>5.3</td>
                                    <td>%</td>
                                    <td>4.0% - 5.6%</td>
                                    <td>Normal</td>
                                </tr>
                            </tbody>
                        </table>
                    </text>
                    <entry typeCode="COMP">
                        <observation classCode="OBS" moodCode="EVN">
                            <code code="903426" codeSystem="2.16.170.11001.1000.14"
                                codeSystemName="CUPS"
                                displayName="HEMOGLOBINA GLICOSILADA AUTOMATIZADA"/>
                            <statusCode code="completed"/>
                            <effectiveTime value="201704061048"/>
                            <value xsi:type="PQ" value="5.3" unit="%"/>
                            <interpretationCode code="N" codeSystem="2.16.840.1.113883.5.83"
                                codeSystemName="ObservationInterpretation" displayName="Normal"/>
                            <referenceRange typeCode="REFV">
                                <observationRange classCode="OBS" moodCode="EVN.CRT">
                                    <value xsi:type="ST">4.0% - 5.6%</value>
                                </observationRange>
                            </referenceRange>
                        </observation>
                    </entry>
                </section>
            </component>
        </structuredBody>
    </component>
</ClinicalDocument>
```


[^1]: Alschuler L, Dolin R, Boyer S. HL7 Clinical Document Architecture, Release 2.0. Health Level Seven®, Inc. Secc 4.3.6  Entry Acts.
[^2]: Health Level Seven®, Inc. HL7 Reference Information Model.
[^3]: IHE International. Laboratory Technical Framework, Volume 1 (LAB TF-1) Profiles. August 8, 2008. Pag 13.
[^4]: Ibid, Pag 11.

