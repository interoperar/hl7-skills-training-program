# CDA Imagenología diagnóstica - Guía de Implementación.

* **Versión del documento:** 0.1 - Prototipo
* **Autor:** Mario Enrique Cortés M <mario.cortes@interoperar.com>
* **Compañía:** Interoperar.
* **Fecha:** 2018-08-07
* **Alcance del proyecto:** Aprendizaje.

[Ver esta guía en formato PDF](https://bitbucket.org/interoperar/hl7-skills-training-program/src/5c343a1fb7893d9de578516c237926249ba3868f/guias-implementacion/pdf/CDA-imagenologia.pdf)

## Definición:

Este documento, proporciona una guía de implementación, de la especificación HL7 CDA R2, adaptada a las necesidades de un escenario de **imagenología diagnóstica**, dentro del marco del proyecto de aprendizaje, denominado **Historia Clínica Electrónica Compartida**.

El contenido ha sido redactado para facilitar la comprensión de desarrolladores o analístas, responsables de la implementación del proyecto.

El alcance de la presente guía es describir la estructura específica de un documento electrónico CDA de reporte diagnóstico de imagenología.  Las características generales (CDA Header y CDA Body) que comparte con los demás tipos de documentos que han sido diseñados por la *Comunidad regional de usuarios de HL7*, han sido abordados en la guía de implementación general [CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

Un documento clínico de laboratorio o CDA de laboratorio es una reporte de pruebas diagnósticas, que cumple con la sintaxis, semántica y reglas de conformidad del estándar HL7 CDA R2.

Su finalidad es el intercambio electrónico de reportes de imagenología diagnóstica.

## Estructura general.

### Encabezado (CDA Header).

#### Nodo raíz:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Identificación unívoca del documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Código del tipo de documento:
* **Definición:** Este elemento es un tipo de dato **CE** (Coded With Equivalents) que contiene atributos de datos codificados que especifican el tipo de documento clínico.  
Por motivos de conformidad con el estándar internacional HL7 CDA R2, se recomienda respetar el uso de el nombre (displayName) en el idioma original del estándar LOINC (Inglés).
* **Nodo Xpath:** /ClinicalDocument/code
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..1
* **Tabla de codificación:** LOINC.
* **Sintaxis:**

```
<code 	code="18748-4" 
		codeSystem="2.16.840.1.113883.6.1" 
		codeSystemName="LOINC" 
		displayName="Diagnostic imaging study"/>
```

#### Título documento:
* **Definición:** Cadena de texto (ST) donde se registra el nombre que la *Comunidad regional de usuarios de HL7* a decidido éste documento.
* **Nodo Xpath:** /ClinicalDocument/title
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplos de sintaxis:**

```
<title>HISTORIA CLINICA ELECTRONICA COMPARTIDA AYUDAS DIAGNOSTICA IMAGENOLOGIA</title>
```

#### Fecha de creación del documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Código de confidencialidad de documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Código de lenguaje de documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Versión del documento:
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Registro Médico (recordTarget):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Autor (author):
* **Definición:** Es un elemento que permite registrar información de la persona que participa como autor del documento clínico (Radiólogo).  
El elemento autor, en escenarios de imagenología diagnóstica, se utiliza un segundo elemento autor (author), para registrar los datos del dispositivo (estación de trabajo) en la cuál se obtuvo la imágen diagnóstica.
* **Nodo Xpath:** /ClinicalDocument/author
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..*
* **Ejemplo de sintaxis:**

```
<author>
    <time value="20180802165000"/>
    <assignedAuthor>
        <id root="2.16.840.1.113883.19.57.2.3.4.1100200001.1.4" 
            extension="SN/WS37412"/>
        <assignedAuthoringDevice classCode="DEV">
            <code code="WSD" 
                codeSystem="1.2.840.10008.2.16.4"
                codeSystemName="DICOM Code Definitions" 
                displayName="Workstation"/>
            <manufacturerModelName>RIS/PACS 001</manufacturerModelName>
            <softwareName>ADVANTAGE RIS</softwareName>
        </assignedAuthoringDevice>
        <representedOrganization>
            <id root="2.16.170.11001.1000.5.1" 
                extension="1100200001"/>
            <name>Diagnóstica IPS</name>
        </representedOrganization>
    </assignedAuthor>
</author>
```

  

[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Custodio (custodian):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Autenticador legal (legalAuthenticator):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Autenticador (authenticator):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

#### Documentación de (documentationOf):
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).

### Cuerpo (CDA Body).
[CDA Clinical Document Architecture](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Encabezado%20-%20Prototipo).


#### Sección (section):
* **Definición:** Es un componente del documento, que agrupa en secciones las solicitudes, hallazgos, impresiones diagnósticas e imágenes (entradas de datos), que componen un reporte de imagenología diagnóstica.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..*
* **Ejemplo de sintaxis:**

```
<component typeCode="COMP" contextConductionInd="true">
    <structuredBody classCode="DOCBODY" moodCode="EVN">
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- SECCIÓN DE PRUEBAS SOLICITADOS -->
                ...
            </section>
        </component>
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- SECCIÓN DE HALLAZGOS -->
                ...
            </section>
        </component>
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- SECCIÓN DE IMPRESIÓN DIAGNÓSTICA -->
                ...
            </section>
        </component>
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- SECCIÓN DE IMÁGENES -->
                ...
            </section>
        </component>
    </structuredBody>
</component>
```

#### Plantillas de las secciones (templateId):
* **Definición:** Este elemento es un tipo de dato **II** (Instance Identifier) que permite el registro de la identificación de una plantilla de la estructura de la sección del documento.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/templateId
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Ejemplos de sintaxis:**

```
<templateId	root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
    		extension="IMRQ_RMDOCSECT20180701BOG01"/>
```

```
<templateId	root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
    		extension="IMGDX_RMDOCSECT20180701BOG01"/>
```

```
<templateId	root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
    		extension="IMGOB_RMDOCSECT20180701BOG01"/>
```

```
<templateId	root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
    		extension="IMGED_RMDOCSECT20180701BOG01"/>
```

#### Código del tipo de sección:
* **Definición:** Este elemento es un tipo de dato **CE** (Coded With Equivalents) que contiene atributos de datos codificados que especifican el tipo de sección dónde se registra el resultado (lectura) del estudio de imagenología diagnóstica.  
Por motivos de conformidad con el estándar internacional HL7 CDA R2, se recomienda respetar el uso de el nombre (displayName) en el idioma original del estándar LOINC (Inglés).
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/code
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Tabla de codificación:** LOINC.
* **Ejemplos de sintaxis:**

```
<code   code="55115-0" 
        codeSystem="2.16.840.1.113883.6.1" 
        codeSystemName="LOINC" 
        displayName="Request (Radiology)" />
```

```
<code   code="18782-3" 
        codeSystem="2.16.840.1.113883.6.1" 
        codeSystemName="LOINC" 
        displayName="Radiology Study observation (findings)" />
```

```
<code   code="19005-8" 
        codeSystem="2.16.840.1.113883.6.1" 
        codeSystemName="LOINC" 
        displayName="Radiology - Impression" />
```

```
<code   code="55113-5" 
        codeSystem="2.16.840.1.113883.6.1" 
        codeSystemName="LOINC" 
        displayName="Key images" />
```

#### Título de la sección:
* **Definición:** Cadena de texto (ST) donde se registra el nombre que la *Comunidad regional de usuarios de HL7* a decidido para cada una de las secciones de los documentos clínicos.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/title
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<title>SOLICITUD IMAGENOLOGIA</title>
```

```
<title>HALLAZGOS IMAGENOLOGIA</title>
```

```
<title>IMPRESIÓN DIAGNÓSTICA IMAGENOLOGIA</title>
```

```
<title>IMÁGENES DIAGNÓSTICAS</title>
```

#### Bloque narrativo de la sección:
* **Definición:** Este elemento es un tipo de dato **ED** (Encapsulated Data) donde se registra la información (no estructurada), del resultado (lectura) del estudio de imagenología diagnóstica, en un un formato similar a XHTML, con el fin de facilitar el procesamiento del documento, para facilitar la legibilidad humana 
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/text
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplos de sintaxis:**


```
<!-- SOLICITUD IMAGENOLOGIA -->
<text mediaType="text/x-hl7-text+xml">
    <table>
        <thead>
            <tr>
                <th>Código</th>
                <th>Estudio solicitado</th>
                <th>Tipo de estudio</th>
                <th>Área corporal</th>
                <th>Prioridad</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>873420</td>
                <td>RADIOGRAFiA DE RODILLA (AP, LATERAL)</td>
                <td>RG</td>
                <td>estructura de región de rodilla derecha</td>
                <td>programado - prioridad</td>
            </tr>
        </tbody>
    </table>
</text>
```

```
<!-- HALLAZGOS IMAGENOLOGIA -->
<text mediaType="text/x-hl7-text+xml">
    <paragraph ID="OBX.1">
        Se observa disminución del espacio 
        inter-articular, derrame del líquido 
        sinovial, inflamación y edema, dolor 
        localizado y desgaste de las carillas 
        articulares
    </paragraph>
</text>
```

```
<!-- IMPRESIÓN DIAGNÓSTICA IMAGENOLOGIA -->
<text mediaType="text/x-hl7-text+xml">
    <table>
        <thead>
            <tr>
                <th>Código</th>
                <th>Diagnóstico</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>M191</td>
                <td>Artrosis postraumatica de otras articulaciones</td>
            </tr>
        </tbody>
    </table>
</text>
```

```
<!-- IMÁGENES DIAGNÓSTICAS -->
<text mediaType="text/x-hl7-text+xml">
    <table>
        <thead>
            <tr>
                <th>Tipo de estudio</th>
                <th>OID</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>RG</td>
                <td>urn:oid:2.16.124.113543.6003.189642796.63084.16748.2599092901</td>
            </tr>
        </tbody>
    </table>
</text>
```


### Entradas de datos estructurados para resultados de imagenología diagnóstica.

De acuerdo con la especificación del estándar HL7 CDA R2 (ANSI/HL7 CDA, R2-2005), las entradas de datos estructurados (entries), representan el componente semánticamente computable de una sección del documento.  Cada sección puede contener cero o varias entradas de datos estructurados [^1].

Las observaciones son uno de los actos principales en las actividades asistenciales. Son utilizadas para soporte diagnóstico, monitoreo de progreso, determinar patrones y otras impresiones diagnósticas de los profesionales de la salud.

De acuerdo con el estándar ANSI/HL7 V3 RIM, R1-2003 (Reference Information Model), las observaciones (observations) permiten el registro de actos que incluyen, entre otros, resultados de imagenología diagnóstica y radiología.
[^2]. 


#### Entradas de datos de reporte de imagenología diagnóstica.

##### Definición:

La imaginología médica y la radiología son especialidades clínicas cuya función es capturar imágenes del interior del cuerpo humano, emplando agentes físicos (utrasonidos, rayos X, campos magnéticos, etc.) y analizar u **observar** estas imágenes para el diagnóstico, pronóstico y el tratamiento de enfermedades. 

De acuerdo con la especificación HL7 V3 **RIM** [^3], la impresión diagnóstica a partir de una imagen clínica es un acto de **Observación**, y en virtud de ello, se utiliza la clase _Observation_ y sus atributos, para el registro de información del análisis que el profesional de la salud hace de dicha imagen.

##### Estructura:


Desde el punto de vista de las estructuras de datos, existen cuatro (4) tipos principales de entradas de datos: 

1. Entradas del (los) tipo(s) de estudio(s) de imagenología realizado(s) (observation).
2. Entradas de hallazgos del estudio de imagenología (Observation).
3. Entradas de impresiones diagnósticas de imagenología (Observation).
4. Entradas de las instancias de imágenes diagnósticas (observationMedia).


###### Entradas del tipo de estudio de imagenología realizado (observation):
* **Definición:** Entrada de datos, altamente estructurada que contiene la descripción de los estudios de imagenología diagnóstica realizados al paciente.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section[1]/entry/observation
* **Requerimiento:** Mandatorio.
* **Ejemplo de sintaxis:**

```
<!-- ***** ENTRADA DE DATOS DE TIPO DE ESTUDIOS REALIZADOS ***** -->
<entry typeCode="COMP" contextConductionInd="true">
    <observation classCode="OBS" moodCode="EVN">
        <!-- Código del tipo observación realizada 
            (estudio por imágenes - acción) -->
        <code code="360037004" codeSystem="2.16.840.1.113883.6.96"
            codeSystemName="SNOMED CT "
            displayName="estudio por imágenes - acción"/>
        <!-- Código de estatus de la observación -->
        <statusCode code="completed"/>
        <!-- Fecha y hora del estudio -->
        <effectiveTime value="20180801095000"/>
        <!-- Código de prioridad de la observación -->
        <priorityCode code="416774000" codeSystem="2.16.840.1.113883.6.96"
            codeSystemName="SNOMED CT " displayName="programado - prioridad"/>
        <!-- Valor: Código del tipo de servicio diagnóstico (observación) realizada -->
        <value xsi:type="CE" code="873420" codeSystem="2.16.170.11001.1000.14"
            codeSystemName="CUPS"
            displayName="RADIOGRAFiA DE RODILLA (AP, LATERAL)"/>
        <!-- Código del tipo de estudio (método) -->
        <methodCode code="RG" codeSystem="1.2.840.10008.2.16.4"
            codeSystemName="DICOM Code Definitions"
            displayName="Radiographic imaging"/>
        <!-- Localización anatómica del estudio -->
        <targetSiteCode code="6757004" codeSystem="2.16.840.1.113883.6.96"
            codeSystemName="SNOMED CT "
            displayName="estructura de región de rodilla derecha"/>
    </observation>
</entry>
```

###### Entradas de hallazgos del estudio de imagenología (Observation):
* **Definición:** Entrada de datos, altamente estructurada que contiene la descripción de los hallazgos clínicos observados por el radiólogo.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section[2]/entry/observation
* **Requerimiento:** Mandatorio.
* **Ejemplo de sintaxis:**

```
<!-- ***** ENTRADA DE DATOS HALLAZGOS ***** -->
<entry typeCode="COMP" contextConductionInd="true">
    <observation classCode="OBS" moodCode="EVN">
        <!-- Código del tipo observación realizada 
            (hallazgo clínico) -->
        <code code="404684003" codeSystem="2.16.840.1.113883.6.96"
            codeSystemName="SNOMED CT" displayName="hallazgo clínico"/>
        <!-- Código de estatus de la observación -->
        <statusCode code="completed"/>
        <!-- Fecha y hora del hallazgo -->
        <effectiveTime value="20180802165000"/>
        <!-- Valor: Código (CIE-10) del hallazgo (diagnóstico) encontrado -->
        <value xsi:type="CE" code="M191" codeSystem="2.16.840.1.113883.6.3"
            codeSystemName="ICD-10"
            displayName="Artrosis postraumatica de otras articulaciones"/>
    </observation>
</entry>
```

###### Entradas de impresiones diagnósticas de imagenología (Observation):
* **Definición:** Entrada de datos, altamente estructurada que contiene la descripción de la impresión diagnóstica (narrativa) del radiólogo.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section[3]/entry/observation
* **Requerimiento:** Mandatorio.
* **Ejemplo de sintaxis:**

```
<!-- ***** ENTRADA DE DATOS DE IMPRESIONES DIAGNÓSTICAS ***** -->
<entry typeCode="COMP" contextConductionInd="true">
    <observation classCode="OBS" moodCode="EVN">
        <!-- Código del tipo observación realizada 
            (hallazgo clínico) -->
        <code code="439401001" codeSystem="2.16.840.1.113883.6.96"
            codeSystemName="SNOMED CT " displayName="diagnóstico"/>
        <statusCode code="completed"/>
        <!-- Fecha y hora del diagnóstico -->
        <effectiveTime value="20180802165000"/>
        <!-- Referencia a la nota de impresión diagnóstica
             registrada en el bloque narrativo de la sección -->
        <value xsi:type="ED">
            <reference value="#OBX.1"/>
        </value>
    </observation>
</entry>
```

###### Entradas de las instancias de imágenes diagnósticas (observationMedia):
* **Definición:** Entrada de datos, altamente estructurada que contiene la referencia a la(s) imagen(es) diagnósticas (DICOM) almacenada(s) en un servidor PACS.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section[4]/entry/observationMedia
* **Requerimiento:** Mandatorio.
* **Ejemplo de sintaxis:**

```
<!-- ***** ENTRADA DE DATOS IMÁGENES DIAGNÓSTICAS ***** -->

<entry typeCode="COMP" contextConductionInd="true">
    <observationMedia classCode="OBS" moodCode="EVN" ID="IMG.1">
        <value mediaType="Application/dicom">
            <!-- ID (OID) de la imágen en el servidor PACS -->
            <reference
                value="urn:oid:2.16.124.113543.6003.189642796.63084.16748.2599092901"/>
        </value>
    </observationMedia>
</entry>
```

## Ejemplo de sintaxis:

A continuación se presenta un ejemplo de un CDA de imagenología diagnóstica.

[Ver archivo fuente](https://bitbucket.org/interoperar/hl7-skills-training-program/src/master/CDA-imagenologia-prototipo-1.xml)

```
<ClinicalDocument classCode="DOCCLIN" moodCode="EVN">
    <typeId root="2.16.840.1.113883.1.3" extension="POCD_HD000040"/>
    <id root="2.16.170.11001.1000.1.110013029428.20170406230458.123.1"
        extension="f76cdf16-8dc0-4954-b474-8ff279f3cb18"/>
    <code code="18748-4" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
        displayName="Diagnostic imaging study"/>
    <effectiveTime value="20180802165000"/>
    <confidentialityCode code="N" codeSystem="2.16.840.1.113883.5.25"
        codeSystemName="Confidentiality" displayName="normal"/>
    <languageCode code="es-CO"/>
    <versionNumber value="1"/>
    <recordTarget typeCode="RCT" contextControlCode="OP">
        <patientRole classCode="PAT">
            <id root="2.16.170.1.1.1.1.1" extension="1679884"/>
            <addr>
                <direction>Avenida Calle 68 # 34-23</direction>
                <city>Bogota D.C.</city>
                <state>Bogota D.C.</state>
                <country>Colombia</country>
            </addr>
            <telecom value="+57(300)111-1111"/>
            <telecom value="nombre@example.com"/>
            <telecom value="+57(1)2222-2222"/>
            <patient classCode="PSN" determinerCode="INSTANCE">
                <id root="2.16.170.1.1.1.1.1" extension="1679884"/>
                <name>
                    <given>Juan</given>
                    <given>Carlos</given>
                    <family>Pernia</family>
                    <family>Ayala</family>
                </name>
                <administrativeGenderCode code="M" codeSystem="2.16.840.1.113883.5.1"
                    codeSystemName="AdministrativeGender" displayName="Male"/>
                <birthTime value="19670328"/>
                <maritalStatusCode code="1" codeSystem="2.16.170.11001.1000.12.3"
                    codeSystemName="EstadoCivil" displayName="soltero"/>
            </patient>
            <providerOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.5" extension="1100130294"/>
                <name> SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E. UNIDAD DE SERVICIOS DE
                    SALUD VISTA HERMOSA </name>
            </providerOrganization>
        </patientRole>
    </recordTarget>
    <author typeCode="AUT" contextControlCode="OP">
        <time value="201704061048"/>
        <assignedAuthor>
            <id root="2.16.170.11001.1000.3.1" extension="89283477"/>
            <assignedPerson classCode="PSN" determinerCode="INSTANCE">
                <name>
                    <given>Federico</given>
                    <family>Iriarte</family>
                    <prefix>Dr.</prefix>
                </name>
            </assignedPerson>
            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
            </representedOrganization>
        </assignedAuthor>
    </author>
    <author>
        <time value="20180802165000"/>
        <assignedAuthor>
            <id root="2.16.840.1.113883.19.57.2.3.4.1100200001.1.4" 
                extension="SN/WS37412"/>
            <assignedAuthoringDevice classCode="DEV">
                <code code="WSD" codeSystem="1.2.840.10008.2.16.4"
                    codeSystemName="DICOM Code Definitions" 
                    displayName="Workstation"/>
                <manufacturerModelName>RIS/PACS 001</manufacturerModelName>
                <softwareName>ADVANTAGE RIS</softwareName>
            </assignedAuthoringDevice>
            <representedOrganization>
                <id root="2.16.170.11001.1000.5.1" extension="1100200001"/>
                <name>Diagnóstica IPS</name>
            </representedOrganization>
        </assignedAuthor>
    </author>
    <custodian typeCode="CST">
        <assignedCustodian classCode="ASSIGNED">
            <representedCustodianOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.5.1" extension="1100130294"
                    assigningAuthorityName="minsalud"/>
                <name> SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E. </name>
            </representedCustodianOrganization>
        </assignedCustodian>
    </custodian>
    <legalAuthenticator typeCode="LA" contextControlCode="OP">
        <time value="201704061048"/>
        <signatureCode code="S"/>
        <assignedEntity classCode="ASSIGNED">
            <id root="2.16.170.11001.1000.3.1" extension="89283477"/>
            <assignedPerson classCode="PSN" determinerCode="INSTANCE">
                <name>
                    <given>Federico</given>
                    <family>Iriarte</family>
                    <prefix>Dr.</prefix>
                </name>
            </assignedPerson>
            <representedOrganization classCode="ORG" determinerCode="INSTANCE">
                <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
            </representedOrganization>
        </assignedEntity>
    </legalAuthenticator>
    <documentationOf typeCode="DOC">
        <serviceEvent classCode="ACT" moodCode="EVN">
            <code code="101" codeSystem="2.16.170.11001.1000.12.2"
                codeSystemName="ServiciosHabilitados" displayName="GENERAL ADULTOS"/>
        </serviceEvent>
    </documentationOf>
    <component typeCode="COMP" contextConductionInd="true">
        <structuredBody classCode="DOCBODY" moodCode="EVN">
            <component typeCode="COMP" contextConductionInd="true">
                <section classCode="DOCSECT" moodCode="EVN">
                    <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
                        extension="IMRQ_RMDOCSECT20180701BOG01"/>
                    <code code="55115-0" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                        displayName="Request (Radiology)"/>
                    <title>SOLICITUD IMAGENOLOGIA</title>
                    <text mediaType="text/x-hl7-text+xml">
                        <table>
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Estudio solicitado</th>
                                    <th>Tipo de estudio</th>
                                    <th>Área corporal</th>
                                    <th>Prioridad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>873420</td>
                                    <td>RADIOGRAFiA DE RODILLA (AP, LATERAL)</td>
                                    <td>RG</td>
                                    <td>estructura de región de rodilla derecha</td>
                                    <td>programado - prioridad</td>
                                </tr>
                            </tbody>
                        </table>
                    </text>
                    <entry typeCode="COMP" contextConductionInd="true">
                        <observation classCode="OBS" moodCode="EVN">
                            <code code="360037004" codeSystem="2.16.840.1.113883.6.96"
                                codeSystemName="SNOMED CT "
                                displayName="estudio por imágenes - acción"/>
                            <statusCode code="completed"/>
                            <effectiveTime value="20180801095000"/>
                            <priorityCode code="416774000" codeSystem="2.16.840.1.113883.6.96"
                                codeSystemName="SNOMED CT " displayName="programado - prioridad"/>
                            <value xsi:type="CE" code="873420" codeSystem="2.16.170.11001.1000.14"
                                codeSystemName="CUPS"
                                displayName="RADIOGRAFiA DE RODILLA (AP, LATERAL)"/>
                            <methodCode code="RG" codeSystem="1.2.840.10008.2.16.4"
                                codeSystemName="DICOM Code Definitions"
                                displayName="Radiographic imaging"/>
                            <targetSiteCode code="6757004" codeSystem="2.16.840.1.113883.6.96"
                                codeSystemName="SNOMED CT "
                                displayName="estructura de región de rodilla derecha"/>
                        </observation>
                    </entry>
                </section>
            </component>
            <component typeCode="COMP" contextConductionInd="true">
                <section classCode="DOCSECT" moodCode="EVN">
                    <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
                        extension="IMGDX_RMDOCSECT20180701BOG01"/>
                    <code code="18782-3" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                        displayName="Radiology Study observation (findings)"/>
                    <title>HALLAZGOS IMAGENOLOGIA</title>
                    <text mediaType="text/x-hl7-text+xml">
                        <paragraph ID="OBX.1"> Se observa disminución del espacio inter-articular,
                            derrame del líquido sinovial, inflamación y edema, dolor localizado y
                            desgaste de las carillas articulares </paragraph>
                    </text>
                    <entry typeCode="COMP" contextConductionInd="true">
                        <observation classCode="OBS" moodCode="EVN">
                            <code code="404684003" codeSystem="2.16.840.1.113883.6.96"
                                codeSystemName="SNOMED CT " displayName="hallazgo clínico"/>
                            <statusCode code="completed"/>
                            <effectiveTime value="20180802165000"/>
                            <value xsi:type="ED">
                                <reference value="#OBX.1"/>
                            </value>
                        </observation>
                    </entry>
                </section>
            </component>
            <component typeCode="COMP" contextConductionInd="true">
                <section classCode="DOCSECT" moodCode="EVN">
                    <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
                        extension="IMGOB_RMDOCSECT20180701BOG01"/>
                    <code code="19005-8" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                        displayName="Radiology - Impression"/>
                    <title>IMPRESIÓN DIAGNÓSTICA IMAGENOLOGIA</title>
                    <text mediaType="text/x-hl7-text+xml">
                        <table>
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Diagnóstico</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>M191</td>
                                    <td>Artrosis postraumatica de otras articulaciones</td>
                                </tr>
                            </tbody>
                        </table>
                    </text>
                    <entry typeCode="COMP" contextConductionInd="true">
                        <observation classCode="OBS" moodCode="EVN">
                            <code code="404684003" codeSystem="2.16.840.1.113883.6.96"
                                codeSystemName="SNOMED CT" displayName="hallazgo clínico"/>
                            <statusCode code="completed"/>
                            <effectiveTime value="20180802165000"/>
                            <value xsi:type="CE" code="M191" codeSystem="2.16.840.1.113883.6.3"
                                codeSystemName="ICD-10"
                                displayName="Artrosis postraumatica de otras articulaciones"/>
                        </observation>
                    </entry>
                </section>
            </component>
            <component typeCode="COMP" contextConductionInd="true">
                <section classCode="DOCSECT" moodCode="EVN">
                    <templateId root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
                        extension="IMGED_RMDOCSECT20180701BOG01"/>
                    <code code="55113-5" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                        displayName="Key images"/>
                    <title>IMÁGENES DIAGNÓSTICAS</title>
                    <text mediaType="text/x-hl7-text+xml">
                        <table>
                            <thead>
                                <tr>
                                    <th>Tipo de estudio</th>
                                    <th>OID</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>RG</td>
                                    <td>urn:oid:2.16.124.113543.6003.189642796.63084.16748.2599092901</td>
                                </tr>
                            </tbody>
                        </table>
                    </text>
                    <entry typeCode="COMP" contextConductionInd="true">
                        <observationMedia classCode="OBS" moodCode="EVN" ID="IMG.1">
                            <value mediaType="Application/dicom">
                                <reference
                                    value="urn:oid:2.16.124.113543.6003.189642796.63084.16748.2599092901"/>
                            </value>
                        </observationMedia>
                    </entry>
                </section>
            </component>
        </structuredBody>
    </component>
</ClinicalDocument>
```



[^1]: Alschuler L, Dolin R, Boyer S. HL7 Clinical Document Architecture, Release 2.0. Health Level Seven®, Inc. Secc 4.3.6  Entry Acts.
[^2]: Health Level Seven®, Inc. HL7 Reference Information Model.
[^3]: Ibid