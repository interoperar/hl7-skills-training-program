# CDA Clinical Document Architecture - Guía de Implementación.

* **Versión del documento:** 0.1 - Prototipo
* **Autor:** Mario Enrique Cortés M <mario.cortes@interoperar.com>
* **Compañía:** Interoperar.
* **Fecha:** 2018-08-07
* **Alcance del proyecto:** Aprendizaje.

[Ver esta guía en formato PDF](https://bitbucket.org/interoperar/hl7-skills-training-program/src/5c343a1fb7893d9de578516c237926249ba3868f/guias-implementacion/pdf/CDA.pdf)

## Definición:

Este documento, proporciona una guía de implementación, de la especificación HL7 CDA R2, adaptada a las necesidades de un proyecto de aprendizaje denominado **Historia Clínica Electrónica Compartida**.

El contenido ha sido redactado para facilitar la comprensión de desarrolladores o analístas, responsables de la implementación del proyecto.

El alcance de la presente guía es describir la estructura general de los documentos CDA (CDA Header y CDA Body).  Las características particulares de cada tipo de documento diseñado, serán abordadas en las guías correpondientes.

De acuerdo con la especificación del estándar HL7 CDA R2, La Arquitectura de Documentos Clínicos o CDA (Clinical Document Architecture) es un estándar de marcado de documentos que especifica la estructura y semántica de "*Documentos Clínicos*", con el propósito de intercambio electrónico [^1].

Un *Documento Clínico* es una recopilación formal de observaciones clínica y servicios asistenciales, con las siguientes características [^2]:

- Persistencia.
- Administración y custodia.
- Potencial de autenticación.
- Contexto.
- Integridad.
- Legibilidad humana
 	 	
Una instancia de documento CDA es definida como un objeto de información completa que puede incluir texto, imágenes, sonidos y otro tipo de contenido multimedia.

Para el proyecto de aprendizaje **Historia Clínica Electrónica Compartida**. se han definido un conjunto de tipos de documentos, a través de un proceso denominado "*localización*", el cual consiste en incluir determinadas restricciones en el diseño, estructura y semántica de la especificación HL7 CDA R2, para adaptarla al contexto del proyecto.

Dicho proceso de localización debería cumplir con el estándar ANSI/HL7 V3 RCL, R2-2007 (HL7 Version 3 Standard: Refinement, Constraint and Localization Release 2).

## Tipos de documentos:

Los tipos de documentos definidos para el proyecto de **Historia Clínica Electrónica Compartida** son:

TÍTULO | CÓDIGO (LOINC) | NOMBRE
:------:|:--------------:|:------:
HISTORIA CLINICA ELECTRONICA COMPARTIDA DE LABORATORIO | 11502-2 | Laboratory report
HISTORIA CLINICA ELECTRONICA COMPARTIDA DE AYUDAS DIAGNOSTICA DE IMAGENOLOGIA | 18748-4 | Diagnostic imaging study
HISTORIA CLINICA ELECTRONICA COMPARTIDA DE VACUNACION | 82593-5 | Immunization summary report.

Para ver las guías de implementación de cada uno de los tipos de documentos, siga los siguientes enlaces:

- HISTORIA CLINICA ELECTRONICA COMPARTIDA DE LABORATORIO [Guía de implementación](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Laboratorio%20-%20Prototipo).
- HISTORIA CLINICA ELECTRONICA COMPARTIDA DE AYUDAS DIAGNOSTICA IMAGENOLOGIA [Guía de implementación](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Imagenolog%C3%ADa%20-%20Prototipo).
- HISTORIA CLINICA ELECTRONICA COMPARTIDA DE VACUNACION [Guía de implementación](https://bitbucket.org/interoperar/hl7-skills-training-program/wiki/CDA%20Vacunaci%C3%B3n%20-%20Prototipo).


## Estructura general.

Un documento CDA esta compuesto por un **Encabezado** (Header) y un **Cuerpo** (Body).

- **Encabezado:** (CDA Header) Están compuesto por un conjunto de metadados denominados  atributos del *ClinicalDocument* (nodo raíz XML), un conjunto de elementos de dato de los *Participantes* (participants) y un conjunto de elementos de datos de los *Actos relacionados* (act relationships).
- **Cuerpo:** (CDA Body) es la estructura que contiene el registro de los actos clínicos principales, de acuerdo con el tipo de documento.

Para la compresión de la estructura, se recomienda consulatar la tabla de Descripción Jerárquica de CDA (CDA Hierarchical Description POCD_HD000040), en formato Microsoft Excel.

### Encabezado (CDA Header).

### Estructura:

#### Nodo raíz:
* **Definición:** Es el elemento principal que contiene los demás elementos de la estructura.
* **Nodo Xpath:** /ClinicalDocument
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<ClinicalDocument classCode="DOCCLIN" moodCode="EVN">
	<typeId root="2.16.840.1.113883.1.3" extension="POCD_HD000040"/>
    ....
</ClinicalDocument>
```

#### Identificación unívoca del documento:
* **Definición:** Es una cadena de texto que permite la identificación unívoca de cada instancia de documento CDA.  
Este elemento es un tipo de dato **II** (Instance Identifier) que contiene los siguientes atributos principales:
	* **Raíz:** (root) Permite el registro de la raíz de la identificación del documento, correspondiente a un OID (raíz de identificador único global)
	* **Extensión:** (extension) Permite el registro del identificador unívoco del recurso.  Se recomienda el uso de un [GUID](https://es.wikipedia.org/wiki/Identificador_único_global).
* **Nodo Xpath:** /ClinicalDocument/id
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..1
* **Ejemplo de sintaxis:**

```
<id root="2.16.170.11001.1000.1.110013029428.20170406230458.123.1" extension="f0eb36f1-d79d-4bde-8de8-c76151755c41"/>
```

#### Código del tipo de documento:
* **Definición:** Este elemento es un tipo de dato **CE** (Coded With Equivalents) que contiene atributos de datos codificados que especifican el tipo de documento clínico.  
Por motivos de conformidad con el estándar internacional HL7 CDA R2, se recomienda respetar el uso de el nombre (displayName) en el idioma original del estándar LOINC (Inglés).
* **Nodo Xpath:** /ClinicalDocument/code
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..1
* **Tabla de codificación:** LOINC.
* **Ejemplos de sintaxis:**

- Sintaxis para el tipo de CDA denominado *HISTORIA CLINICA ELECTRONICA COMPARTIDA LABORATORIO*:

```
<code	code="11502-2" 
		codeSystem="2.16.840.1.113883.6.1" 
		codeSystemName="LOINC" 
		displayName="Laboratory report"/>
```

- Sintaxis para el tipo de CDA denominado *HISTORIA CLINICA ELECTRONICA COMPARTIDA AYUDAS DIAGNOSTICA IMAGENOLOGIA*:

```
<code 	code="18748-4" 
		codeSystem="2.16.840.1.113883.6.1" 
		codeSystemName="LOINC" 
		displayName="Diagnostic imaging study"/>
```

- Sintaxis para el tipo de CDA denominado *HISTORIA CLINICA ELECTRONICA COMPARTIDA VACUNACION*:

```
<code 	code="82593-5" 
		codeSystem="2.16.840.1.113883.6.1" 
		codeSystemName="LOINC" 
		displayName="Immunization summary report"/>
```

#### Título documento:
* **Definición:** Cadena de texto (ST) donde se registra el nombre que la *Comunidad regional de usuarios de HL7* a decidido para éstos documentos.
* **Nodo Xpath:** /ClinicalDocument/title
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplos de sintaxis:**

```
<title>HISTORIA CLINICA ELECTRONICA COMPARTIDA LABORATORIO</title>
```

```
<title>HISTORIA CLINICA ELECTRONICA COMPARTIDA AYUDAS DIAGNOSTICA IMAGENOLOGIA</title>
```

```
<title>HISTORIA CLINICA ELECTRONICA COMPARTIDA VACUNACION</title>
```

#### Fecha de creación del documento:
* **Definición:** Este elemento es un tipo de dato **TS** (Point in Time), que contiene la fecha y hora de generación del registro original de información de salud. 
* **Nodo Xpath:** /ClinicalDocument/effectiveTime
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 0..1
* **Nota:** Formato de fecha (TS) AAAAMMDDhhmmss
* **Ejemplos de sintaxis:**

```
<effectiveTime value="201704061048"/>
```

#### Código de confidencialidad de documento:
* **Definición:** Este elemento es un tipo de dato **CE** (Coded With Equivalents) que contiene atributos de datos codificados que especifican el nivel de confidencialidad del contenido del documento clínico.
* **Nodo Xpath:** /ClinicalDocument/confidentialityCode
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..1
* **Tabla de codificación:** Confidentiality HL7 v3.
* **Ejemplos de sintaxis:**

- Sintaxis para nivel de confidencialidad Normal (valor por defecto):

```
<confidentialityCode	code="N" 
						codeSystem="2.16.840.1.113883.5.25" 
						codeSystemName="Confidentiality" 
						displayName="normal"/>
```

- Sintaxis para nivel de confidencialidad restringida:

```
<confidentialityCode 	code="R" 
						codeSystem="2.16.840.1.113883.5.25" 
						codeSystemName="Confidentiality" 
						displayName="restricted"/>
```

- Sintaxis para nivel de confidencialidad muy restringida:

```
<confidentialityCode 	code="V" 
						codeSystem="2.16.840.1.113883.5.25" 
						codeSystemName="Confidentiality" 
						displayName="very restricted"/>
```

#### Código de lenguaje de documento:
* **Definición:** Este elemento es un tipo de dato **CS** (Coded Simple Value) que especifica el lenguaje (es-CO) de la información registrada en el  documento clínico.
* **Nodo Xpath:** /ClinicalDocument/languageCode
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Tabla de codificación:** RFC 3066.
* **Ejemplos de sintaxis:**

```
<languageCode code="es-CO"/>
```

#### Versión del documento:
* **Definición:** Este elemento es un tipo de dato **INT** (Integer Number) que especifica la versión de la instancia del documento clínico.  
Este dato permite la trazabilidad de actualizaciones del documento.  
El valor de este elemento de dato (1, 2, 3, 4, etc.) es asignado por el servidor de repositorio de documentos clínicos, no por el sistema que genera el documento.
* **Nodo Xpath:** /ClinicalDocument/versionNumber
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplos de sintaxis:**

```
<versionNumber value="1" />
```

#### Registro Médico (recordTarget):
* **Definición:** Es un elemento que permite registrar información del registro médico (recordTarget) al que pertenece el documento.
El registro médico (recordTarget) representa la relación entre una persona y una organización, donde la persona tiene un rol de paciente (clase PatientRole); y la organización tiene el rol de Institución Prestadora de Servicios de Salud (clase providerOrganization).
* **Nodo Xpath:** /ClinicalDocument/recordTarget
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..*

##### Estructura del Registro Médico:

El registro médico (recordTarget) está compuesto por los siguientes elementos:


###### Número de Historia Clínica
* **Definición:** Identificación de la persona en el rol de paciente (Número  o ID de Historia Clínica).
*  **Nodo Xpath:** /ClinicalDocument/recordTarget/patientRole/id
* **Requerimiento:** Mandatorio
* **Cardinalidad:** 1..*
* _Nota:_ Se requiere la asignación de una raíz OID para las historias clínicas de los pacientes bajo la jurisdicción de la Comunidad regional de usuarios de HL7.
* **Ejemplo de sintaxis:**

```
<id root="2.16.170.1.1.1.1.1" extension="1679884"/>
```


###### Dirección del paciente
* **Definición:** Dirección del paciente.
*  **Nodo Xpath:** /ClinicalDocument/recordTarget/patientRole/addr
* **Requerimiento:** Opcional
* **Cardinalidad:** 0..*
* **Ejemplo de sintaxis:**

```
<addr>
    <direction>Avenida Calle 68 # 34-23</direction>
    <city>Bogota D.C.</city>
    <state>Bogota D.C.</state>
    <country>Colombia</country>
</addr>
```

###### Dirección de telecomunicaciones
* **Definición:** Teléfono, correo electrónico, móvil u otra dirección de telecomunicaciones.
* **Nodo Xpath:** /ClinicalDocument/recordTarget/patientRole/telecom
* **Requerimiento:** Opcional
* **Cardinalidad:** 0..*
* **Ejemplo de sintaxis:**

```
<!-- Sintaxis recomendada para teléfono móvil -->
<telecom value="+57(300)111-1111"/>
            
<!-- Sintaxis recomendada para correo electrónico -->
<telecom value="nombre@example.com"/>
            
<!-- Sintaxis recomendada para teléfono fijo -->
<telecom value="+57(1)2222-2222"/>
```

###### Número de identificación
* **Definición:** Documento de identificación de la persona (CC, PA, TI, etc).
* **Nodo Xpath:** /ClinicalDocument/recordTarget/patientRole/patient/id
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7)
* **Cardinalidad:** 0..1

* _Nota:_ Si bien la especificación HL7 CDA R2, considera este elemento como *Obsoleto*, se conserva dentro de la estructura, para el registro de la identificación de la persona que desempeña el rol de paciente.

* _Nota:_ se recomienda el uso de las siguiente raíces OID para la Tabla de tipos de documentos de identificación de personas:

OID (root) | Abreviatura | Tipo de documento de identidad
-----------|:-----------:|-------------------------------
2.16.170.1.1.1.1.1|CC| Cédula ciudadanía
2.16.170.1.1.1.1.2|CE| Cédula de extranjería
2.16.170.1.1.1.1.3|PA| Pasaporte
2.16.170.1.1.1.1.4|RC| Registro civil
2.16.170.1.1.1.1.5|TI| Tarjeta de identidad
2.16.170.1.1.1.1.6|AS| Adulto sin identificación
2.16.170.1.1.1.1.7|MS| Menor sin identificación
2.16.170.1.1.1.1.8|NU| Número único de identificación
2.16.170.1.1.1.1.9|PE| Permiso Especial

* **Ejemplos de sintaxis:**

```
	<!-- Sintaxis recomendada para CC Cédula ciudadanía -->
	<id root="2.16.170.1.1.1.1.1" extension="1679884"/>
	
	<!-- Sintaxis recomendada para CE Cédula de extranjería -->
	<id root="2.16.170.1.1.1.1.2" extension="1679884"/>
	
	<!-- Sintaxis recomendada para PA Pasaporte -->
	<id root="2.16.170.1.1.1.1.3" extension="1679884"/>
	
	<!-- Sintaxis recomendada para RC Registro civil -->
	<id root="2.16.170.1.1.1.1.4" extension="1679884"/>
	
	<!-- Sintaxis recomendada para TI Tarjeta de identidad -->
	<id root="2.16.170.1.1.1.1.5" extension="1679884"/>
	
	<!-- Sintaxis recomendada para AS Adulto sin identificación -->
	<id root="2.16.170.1.1.1.1.6" extension="1679884"/>
	
	<!-- Sintaxis recomendada para MS Menor sin identificación -->
	<id root="2.16.170.1.1.1.1.7" extension="1679884"/>
	
	<!-- Sintaxis recomendada para NU Número único de identificación -->
	<id root="2.16.170.1.1.1.1.8" extension="1679884"/>
	
	<!-- Sintaxis recomendada para PE Permiso Especial -->
	<id root="2.16.170.1.1.1.1.9" extension="1679884"/>
```

###### Nombre
* **Definición:** Nombres y apellidos de la persona que desempeña el rol de paciente.
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Nodo Xpath:** /ClinicalDocument/recordTarget/patientRole/patient/name
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..*
* **Ejemplo de sintaxis:**

```
<name>
    <given>Juan</given>
    <given>Carlos</given>
    <family>Pernia</family>
    <family>Ayala</family>
</name>
```

###### Género
* **Definición:** Género de la persona que desempeña el rol de paciente.
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Nodo Xpath:** /ClinicalDocument/recordTarget/patientRole/patient/administrativeGenderCode
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Tabla de codificación:** AdministrativeGender (2.16.840.1.113883.5.1)

Código | Género
-------|-------
F|Female
M|Male
UN| Undifferentiated
* **Ejemplo de sintaxis:**


```
<administrativeGenderCode	code="M" 
							codeSystem="2.16.840.1.113883.5.1" 
							codeSystemName="AdministrativeGender" 
							displayName="Male"/>
```

###### Fecha de nacimiento
* **Definición:** Fecha de nacimiento de la persona que desempeña el rol de paciente.
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Nodo Xpath:** /ClinicalDocument/recordTarget/patientRole/patient/birthTime
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<birthTime value="19670328"/>
```

###### Estado civíl
* **Definición:** Estado civíl de la persona que desempeña el rol de paciente.
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Nodo Xpath:** /ClinicalDocument/recordTarget/patientRole/patient/maritalStatusCode
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Tabla de codificación:** EstadoCivil (2.16.170.11001.1000.12.3).

Código | Estado civíl
:-----:|-------------
1|Soltero(a)2|Casado(a)3|Divorciado(a)4|Separado (a)5|Viudo(a)6|Unión Libre9|Sin Dato

* _Nota:_ Se asume que el OID asignado por la Comunidad regional de usuarios de HL7 para la codificación *EstadoCivil* es **2.16.170.11001.1000.12.3**.
* **Ejemplo de sintaxis:**

```
<maritalStatusCode	code="1" 
					codeSystem="2.16.170.11001.1000.12.3" 
					codeSystemName="EstadoCivil" 
					displayName="soltero"/>
```

###### Organización prestadora de servicios de salud
* **Definición:** Elemento perteneciente al registro médico (RecordTarget), para los datos de la organización tiene el rol de Institución Prestadora de Servicios de Salud (clase providerOrganization), donde se originó el documento clínico.
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Nodo Xpath:** /ClinicalDocument/recordTarget/patientRole/providerOrganization
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<providerOrganization classCode="ORG" determinerCode="INSTANCE">
    <id  root="2.16.170.11001.1000.5" extension="1100130294"/>
    <name>
    	SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.
        UNIDAD DE SERVICIOS DE SALUD VISTA HERMOSA
     </name>
</providerOrganization>
```

#### Autor (author):
* **Definición:** Es un elemento que permite registrar información de la(s) persona(s) que participa(n) como autor(es) del documento clínico (Médico o profesional de la salud).
* **Nodo Xpath:** /ClinicalDocument/author
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..*
* **Ejemplo de sintaxis:**


```
<author typeCode="AUT" contextControlCode="OP">
    <!-- Fecha y hora de la participación del autor en el registro médico -->
    <time value="201704061048"/>
    <assignedAuthor>
        <!-- Identificación profesional del autor del documento (RETHUS) -->
        <id root="2.16.170.11001.1000.3.1" extension="19343111" />
        <assignedPerson classCode="PSN" determinerCode="INSTANCE">
            <!-- Nombre del autor -->
            <name>
                <given>Jacinto</given>
                <family>Carrillo</family>
                <prefix>Dr.</prefix>
            </name>
        </assignedPerson>
        <!-- Organización (punto de atención) a la cual representa el autor -->
        <representedOrganization classCode="ORG" determinerCode="INSTANCE">
            <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
        </representedOrganization>
    </assignedAuthor>
</author>
```

#### Custodio (custodian):
* **Definición:** Es un elemento que permite registrar información de laorganización que participa como responsable de la custodia del documento clínico (Sub Red de Atención Integral en Salud).
* **Nodo Xpath:** /ClinicalDocument/custodian
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..1
* **Ejemplo de sintaxis:**

```
<custodian typeCode="CST">
    <assignedCustodian classCode="ASSIGNED">
        <representedCustodianOrganization classCode="ORG" determinerCode="INSTANCE">
            <id root="2.16.170.11001.1000.5.1" 
                extension="1100130294"
                assigningAuthorityName="minsalud"/>
            <name>
                SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.
            </name>
        </representedCustodianOrganization>
    </assignedCustodian>
</custodian>
```

#### Autenticador legal (legalAuthenticator):
* **Definición:** Es un elemento que permite registrar información la persona que firma legalmente el documento clínico (generalmente el autor principal).  
Un autenticador legal o firmante, es una persona que asume el rol desginado por la Institución Prestadora de Servicios de Salud, de firmar el registro clínico, asumiendo inicialmente la responsabilidad legal por su contenido.  
Por ejemplo, en el caso de estudiantes residentes de medicina que en muchos casos participan como múltiples autores del documento, es el médico especialista quien asume la responsabilidad principal de firmar el documento clínico.
* **Nodo Xpath:** /ClinicalDocument/legalAuthenticator
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<legalAuthenticator typeCode="LA" contextControlCode="OP">
    <!-- Fecha y hora de la participación del firmante del documento  -->
    <time value="201704061048"/>
    <signatureCode code="S"/>
    <assignedEntity classCode="ASSIGNED">
        <!-- Identificación profesional que firma el documento (RETHUS) -->
        <id root="2.16.170.11001.1000.3.1" extension="19343111" />
        <assignedPerson classCode="PSN" determinerCode="INSTANCE">
            <!-- Nombre del firmante -->
            <name>
                <given>Jacinto</given>
                <family>Carrillo</family>
                <prefix>Dr.</prefix>
            </name>
        </assignedPerson>
        <!-- Organización (punto de atención) a la cual representa el firmante -->
        <representedOrganization classCode="ORG" determinerCode="INSTANCE">
            <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
        </representedOrganization>
    </assignedEntity>
</legalAuthenticator>
```

#### Autenticador (authenticator):
* **Definición:** Es un elemento que permite registrar información de la persona copartícipe en la firma del documento clínico (generalmente los demás autores o autores de secciones del documento).  
Un autenticador o co-firmante, es una persona que asume el rol desginado por la Institución Prestadora de Servicios de Salud, de firmar parte del registro clínico, asumiendo la co-responsabilidad legal por su contenido.  
* **Nodo Xpath:** /ClinicalDocument/authenticator
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..*
* **Ejemplo de sintaxis:**

```
<authenticator typeCode="AUTHEN">
    <!-- Fecha y hora de la participación del co-firmante del documento  -->
    <time value="201704061048"/>
    <signatureCode code="S"/>
    <assignedEntity classCode="ASSIGNED">
        <!-- Identificación profesional co-firmante del documento (RETHUS) -->
        <id root="2.16.170.11001.1000.3.1" extension="384728374" />
        <assignedPerson classCode="PSN" determinerCode="INSTANCE">
            <!-- Nombre del co-firmante -->
            <name>
                <given>Gregorio</given>
                <family>Casas</family>
                <prefix>Dr.</prefix>
            </name>
        </assignedPerson>
        <!-- Organización (punto de atención) a la cual representa el co-firmante -->
        <representedOrganization classCode="ORG" determinerCode="INSTANCE">
            <id root="2.16.170.11001.1000.4.128" extension="1100130294"/>
        </representedOrganization>
    </assignedEntity>
</authenticator>
```

#### Documentación de (documentationOf):
* **Definición:** Es un elemento que permite registrar información de los servicios (actos) de los cuales hace parte el documento clínico. 
* **Nodo Xpath:** /ClinicalDocument/documentationOf
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..*
* **Ejemplo de sintaxis:**

```
<documentationOf typeCode="DOC">
    <serviceEvent classCode="ACT" moodCode="EVN">
        <code code="101" codeSystem="2.16.170.11001.1000.12.2"
            codeSystemName="ServiciosHabilitados" displayName="GENERAL ADULTOS"/>
    </serviceEvent>
</documentationOf>
```

### Cuerpo (CDA Body).

Existen dos (2) tipos de estructura de contenido del cuerpo de un documento CDA.

- **Cuerpo no estructurado:** (nonXMLBody) que contiene un documento clínico  en un formato diferente a XML (ej: un documento en formato PDF).  
Dicho contenido puede estar dentro del documento (encapsulado empleando codificación Base 64) o referenciado a una URL absoluta.

- **Cuerpo estructurado:** (structuredBody) que contiene los detalles de los registros clínicos, organizados en secciones, bloques narrativos y entradas de datos estructuradas.

### Estructura:

#### Sección (section):
* **Definición:** Es un componente del documento, que agrupa entradas de datos.  
Las secciónes puede subdividirse en forma recurrente en otras secciones, para formar estructuras complejas de agrupamiento de entradas de datos.

* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section
* **Requerimiento:** Mandatorio.
* **Cardinalidad:** 1..*
* **Ejemplo de sintaxis:**

```
<component typeCode="COMP" contextConductionInd="true">
    <structuredBody classCode="DOCBODY" moodCode="EVN">
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- Sección 1 del documento -->
                ...
            </section>
        </component>
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- Sección 2 del documento -->
                ...
            </section>
        </component>
        ...
        <component typeCode="COMP" contextConductionInd="true">
            <section classCode="DOCSECT" moodCode="EVN">
                <!-- Sección n del documento -->
                ...
            </section>
        </component>
    </structuredBody>
</component>
```

#### Plantilla de la sección (templateId):
* **Definición:** Este elemento es un tipo de dato **II** (Instance Identifier) que permite el registro de la identificación de una plantilla de la estructura de la sección del documento.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/templateId
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<templateId	root="2.16.170.2.3.2.3.11001.1.5.3.1.1"
    		extension="POCD_RMDOCSECT20180701BOG01"/>
```

#### Código del tipo de sección:
* **Definición:** Este elemento es un tipo de dato **CE** (Coded With Equivalents) que contiene atributos de datos codificados que especifican el tipo de sección del documento clínico.  
Por motivos de conformidad con el estándar internacional HL7 CDA R2, se recomienda respetar el uso de el nombre (displayName) en el idioma original del estándar LOINC (Inglés).
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/code
* **Requerimiento:** Opcional.
* **Cardinalidad:** 0..1
* **Tabla de codificación:** LOINC.
* **Ejemplos de sintaxis:**

```
<code code="41291-6" 
    codeSystem="2.16.840.1.113883.6.1" 
    codeSystemName="LOINC" 
    displayName="History of Immunization"/>
```

```
<code code="18725-2" 
    codeSystem="2.16.840.1.113883.6.1" 
    codeSystemName="LOINC" 
    displayName="Microbiology studies"/>
```


#### Título documento:
* **Definición:** Cadena de texto (ST) donde se registra el nombre que la *Comunidad regional de usuarios de HL7* a decidido cada una de las secciones de los documentos clínicos.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/title
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplos de sintaxis:**

```
<title>ESQUEMA DE VACUNACION</title>
```

```
<title>PRUEBAS DIAGNÓSTICAS DE QUÍMICA</title>
```

#### Bloque narrativo de la sección:
* **Definición:** Este elemento es un tipo de dato **ED** (Encapsulated Data) donde se registra la información (no estructurada) en un un formato similar a XHTML, con el fin de facilitar el procesamiento del documento, para facilitar la legibilidad humana 
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/text
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..1
* **Ejemplo de sintaxis:**

```
<text mediaType="text/x-hl7-text+xml">
    <table>
        <thead>
            <tr>
                <th>Vacuna</th>
                <th>Dosis</th>
                <th>Fecha</th>
                <th>Nombre comercial</th>
                <th>Lote</th>
                <th>Institución vacunadora</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>vacuna antirrábica</td>
                <td>Primera dosis</td>
                <td>20150606</td>
                <td>Imovax</td>
                <td>LT3428</td>
                <td>SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.</td>
            </tr>
            <tr>
                <td>vacuna antirrábica</td>
                <td>Segunda dosis</td>
                <td>20150613</td>
                <td>Imovax</td>
                <td>LT3478</td>
                <td>SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.</td>
            </tr>
            <tr>
                <td>vacuna antirrábica</td>
                <td>Tercera dosis</td>
                <td>20150704</td>
                <td>Imovax</td>
                <td>LT3490</td>
                <td>SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.</td>
            </tr>
        </tbody>
    </table>
</text>
```

#### Entradas de datos estructurados:
* **Definición:** Este elemento permite el registro estructurado de los diferentes actos clínicos (observaciones, procedimientos, administración de substancias, etc), mediante entradas de datos (Entries).  
Cada sección puede contener múltiples entradas de datos, cada uno de ellos, correspondiente a un tipo de acto específico.
* **Nodo Xpath:** /ClinicalDocument/component/structuredBody/component/section/entry/act
* **Requerimiento:** Opcional (recomendado por la Comunidad regional de usuarios de HL7).
* **Cardinalidad:** 0..*
* **Ejemplo de sintaxis:**

```
<entry typeCode="COMP">
    <observation classCode="OBS" moodCode="EVN">
        <code 	code="903426" 
            codeSystem="2.16.170.11001.1000.14" 
            codeSystemName="CUPS" 
            displayName="HEMOGLOBINA GLICOSILADA AUTOMATIZADA"/>
        <statusCode code="completed"/>
        <effectiveTime value="20180802165000"/>
        <value xsi:type="PQ" value="5.3" unit="%" />
        <interpretationCode	code="N" 
            codeSystem="2.16.840.1.113883.5.83" 
            codeSystemName="ObservationInterpretation" 
            displayName="Normal"/>
        <referenceRange typeCode="REFV">
            <observationRange classCode="OBS" moodCode="EVN.CRT">
                <value xsi:type="ST">4.0% - 5.6%</value>
            </observationRange>
        </referenceRange>
    </observation>
</entry>
```


[^1]: Alschuler L, Dolin R, Boyer S. HL7 Clinical Document Architecture, Release 2.0. Health Level Seven®, Inc. Secc 1.1 What is the CDA.
[^2]: Ibid